﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class Login : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        //ATRIBUTO
        Boolean _Autorizado = false;
        //PROPIEDAD DE SOLO LECTURA
        public Boolean Autorizado
        {
            get { return _Autorizado; }
            //set { _Autorizado = value; }
        }

        private void Validar(String pUsuario,String pCredencial)
        {
            DataTable DatosUsuario = new DataTable();
            try
            {
                DatosUsuario = CacheManager.SystemCache.ValidarUsuario(pUsuario, pCredencial);
                if(DatosUsuario.Rows.Count==1)
                {
                    _Autorizado = true;
                    _SESION.OUsuario.IDUsuario = DatosUsuario.Rows[0]["idUsuario"].ToString();
                    _SESION.OUsuario.NombreUsuario = DatosUsuario.Rows[0]["usuario"].ToString();
                    _SESION.OUsuario.Rol = DatosUsuario.Rows[0]["nombreRol"].ToString();
                    _SESION.OUsuario.IDRol = DatosUsuario.Rows[0]["idRol"].ToString();
                    _SESION.OUsuario.IDEmpleado = DatosUsuario.Rows[0]["idEmpleado"].ToString();
                    _SESION.OUsuario.Empleado = DatosUsuario.Rows[0]["Empleado"].ToString();

                    //OBTENEMOS LOS PERMISOS DE USUARIOS
                    _SESION.OUsuario.ObtenerInfo();
                    Close();
                }else
                {
                    _Autorizado = false;
                    lblMensaje.Text = "USUARIO / CREDENCIAL ERRÓNEOS";
                }
            }
            catch { }
        }

        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Validar(txbUsuario.Text, txbCredencial.Text);
        }

        private void lblMensaje_Click(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Validar(txbUsuario.Text, txbCredencial.Text);
            }
        }

        private void txbCredencial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Validar(txbUsuario.Text, txbCredencial.Text);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
