﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.GUI
{
    public partial class Principal : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;

        public Principal()
        {
            InitializeComponent();
        }
        private void comprobarConexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GUI.DBPruebas f = new DBPruebas();
            f.Show();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = _SESION.OUsuario.NombreUsuario;
        }

        private void gestioEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //El parametro que solicita la funcion VerficaPermiso en el idOpcion
            if (_SESION.OUsuario.VerificarPermiso(2))
            {
                AbrirEnPanel(new General.GUI.GestiondeEmpleados());
                General.GUI.GestiondeEmpleados f = new General.GUI.GestiondeEmpleados();
                f.MdiParent = this;
                f.Show();
            }

        }
        //Este metodo se hace para hacer que el formulario aparezca en el panel contenedor
        private void AbrirEnPanel(object formHijo)
        {
            if (this.panelContenedor.Controls.Count > 0)

                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = formHijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();

        }

        private void gestionDeUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_SESION.OUsuario.VerificarPermiso(3))
            {
                AbrirEnPanel(new General.GUI.GestionUsuarios());
                General.GUI.GestionUsuarios f = new General.GUI.GestionUsuarios();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            AbrirEnPanel(new General.GUI.GestionProductos());
            General.GUI.GestionProductos f = new General.GUI.GestionProductos();
            f.MdiParent = this;
            f.Show();
        }

        private void generalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
               
        private void btnEncargos_Click(object sender, EventArgs e)
        {
            if (_SESION.OUsuario.VerificarPermiso(4))
            {
                AbrirEnPanel(new General.GUI.GestionEncargos());
                General.GUI.GestionEncargos f = new General.GUI.GestionEncargos();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            AbrirEnPanel(new General.GUI.GestionarVentas());
            General.GUI.GestionarVentas f = new General.GUI.GestionarVentas();
            f.MdiParent = this;
            f.Show();
        }
    }
}
