﻿using FastFoodDemo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sistema.CLS
{
    class AppManager:ApplicationContext
    {

        private void Splash()
        {
            GUI.Splash f = new GUI.Splash();
            f.ShowDialog();
        }
        private Boolean Login()
        {
            Boolean Autorizado = false;
            GUI.Login f = new GUI.Login();
            f.ShowDialog();
            Autorizado = f.Autorizado;
            return Autorizado;
        }
        public AppManager()
        {
            Splash();
            if(Login())
            {
                //CUANDO EL USUARIO INGRESA CORRECTAMENTE SUS CREDENCIALES
                FastFoodDemo.frmPrincipal f = new frmPrincipal();
                f.ShowDialog();
            }
            else
            {
                //CUANDO EL USUARIO DECIDE SALIRSE

            }
        }
    }
}
