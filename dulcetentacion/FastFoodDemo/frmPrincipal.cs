﻿using General.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FastFoodDemo
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
            SidePanel.Height = btnHome.Height;
            SidePanel.Top = btnHome.Top;
            AbrirFormulario<Home>();
        }

        private void AbrirFormulario<MiForm>() where MiForm : Form, new()
        {
            Form formulario;
            formulario = panelformularios.Controls.OfType<MiForm>().FirstOrDefault();//Busca en la colecion el formulario
            //si el formulario/instancia no existe
            if (formulario == null)
            {
                formulario = new MiForm();
                formulario.TopLevel = false;
                formulario.FormBorderStyle = FormBorderStyle.None;
                formulario.Dock = DockStyle.Fill;
                panelformularios.Controls.Add(formulario);
                panelformularios.Tag = formulario;
                formulario.Show();
                formulario.BringToFront();
                formulario.FormClosed += new FormClosedEventHandler(CloseForms);
            }
            //si el formulario/instancia existe
            else
            {
                formulario.BringToFront();
            }
        }

        private void CloseForms(object sender, FormClosedEventArgs e)
        {
            
        }

        //METODO PARA ARRASTRAR EL FORMULARIO---------------------------------------------------------------------
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);



        private void button1_Click(object sender, EventArgs e)
        {
            AbrirFormulario<Home>();
            SidePanel.Height = btnHome.Height;
            SidePanel.Top = btnHome.Top;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestionProductos>();
            SidePanel.Height = button2.Height;
            SidePanel.Top = button2.Top;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            AbrirFormulario<General.GUI.GestionarVentas>();
            SidePanel.Height = button3.Height;
            SidePanel.Top = button3.Top;
             
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestionEncargos>();
            SidePanel.Height = button4.Height;
            SidePanel.Top = button4.Top;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestionCompras>();
            SidePanel.Height = button5.Height;
            SidePanel.Top = button5.Top;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SidePanel.Height = button6.Height;
            SidePanel.Top = button6.Top;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.BuscarCliente>();
            SidePanel.Height = button7.Height;
            SidePanel.Top = button7.Top;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            btnMaximizar.Visible = false;
            btnRestaurar.Visible = true;
            btnCerrar.Visible = true ;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }
        //Capturar posicion y tamaño antes de maximizar para restaurar
        int lx, ly;
        int sw, sh;
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            btnMaximizar.Visible = false;
            btnRestaurar.Visible = true;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            btnMaximizar.Visible = true;
            btnRestaurar.Visible = false;
            this.Size = new Size(sw, sh);
            this.Location = new Point(lx, ly);
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            BuscarProducto v = new BuscarProducto();
            v.ShowDialog();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestiondeEmpleados>();
            SidePanel.Height = btnEmpleados.Height;
            SidePanel.Top = btnEmpleados.Top;
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestionUsuarios>();
            SidePanel.Height = btnUsuarios.Height;
            SidePanel.Top = btnUsuarios.Top;
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnReportes.Height;
            SidePanel.Top = btnReportes.Top;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            AbrirFormulario<General.GUI.GestionPermisos>();
        }



    }
}
