﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace SessionManager
{
    public class Usuario
    {

        DataTable _Permisos;
        String _IDUsuario;
        String _Usuario;
        String _IDRol;
        String _IDEmpleado;
        String _Empleado;
        String _Rol;

        public String Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }

        public String Empleado
        {
            get { return _Empleado; }
            set { _Empleado = value; }
        }

        public String IDEmpleado
        {
            get { return _IDEmpleado; }
            set { _IDEmpleado = value; }
        }

        public String IDRol
        {
            get { return _IDRol; }
            set { _IDRol = value; }
        }


        public String NombreUsuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public String IDUsuario
        {
            get { return _IDUsuario; }
            set { _IDUsuario = value; }
        }
        private void cargarPermisos()
        {
            _Permisos = CacheManager.SystemCache.PERMISOS_DE_USUARIOS(_Usuario);

        }
        public void ObtenerInfo()
        {
            cargarPermisos();
        }

        //Para coordinar los permisos de usuario

        public Boolean VerificarPermiso(Int32 pIDPermiso)
        {
            //Le consultamos al dataset por una fila(permiso) en esapecifico
            Boolean Autorizado = false;
            
            //Aqui queremos solo consultar, no ver el registro del permiso
            //Tratar de optmizar este metodo de recorrido(probar con un bindingSouerce)
            foreach (DataRow Fila in _Permisos.Rows)
            {
                //Verificar si asi se llama en la base
                if (Fila["idOpcion"].ToString().Equals(pIDPermiso.ToString()))
                {
                    Autorizado = true;
                    break;
                }
            }
            if (Autorizado == false)
            {
                MessageBox.Show("No tiene el permiso para esta operacion");
            }
            return Autorizado;

        }
    }

}

