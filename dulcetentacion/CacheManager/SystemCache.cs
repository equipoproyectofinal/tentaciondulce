﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CacheManager
{
    public static class SystemCache
    {
        public static DataTable ValidarUsuario(String pUsuario, String pCredencial)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT a.idUsuario,
            a.usuario,
            a.idRol,
            b.nombreRol,
            a.idEmpleado,
            CONCAT(c.nombres,' ',c.apellidos) as 'Empleado'
            FROM usuarios a, roles b, empleados c
            WHERE a.idRol=b.idRol
            AND a.idEmpleado=c.idEmpleado
            AND a.usuario='"+pUsuario+@"'
            AND a.Credencial=sha1('"+pCredencial+"');");

            
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
            }

            return Resultado;
        }
        public static DataTable TODOS_LOS_EMPLEADOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idEmpleado, nombres, apellidos, 
             dui, direccion, telefono, fechaNacimiento, nit from empleados
             Order by apellidos, nombres;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();
                
            }

            return Resultado;
        }
        public static DataTable TODOS_LOS_USUARIOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT a.idUsuario, a.usuario, 
             b.idEmpleado, b.nombres, c.idRol, a.Estado from usuarios a, empleados b, roles c
             where a.idEmpleado=b.idEmpleado and a.idRol=c.idRol
             Order by idUsuario;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable TODOS_LOS_PRODUCTOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idProducto, nombreProducto, precioEntrada, 
                               precioSalida, tipoProducto, iva from productos;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable SOLO_PRODUCTOS_TERMINADOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idProducto, nombreProducto, precioSalida,iva 
            FROM productos WHERE tipoProducto='PRODUCTO TERMINADO' ORDER BY nombreProducto;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        

        public static DataTable ProductosEncargo(String idEncargo)
        {
            
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT pr.idProducto, pr.nombreProducto, pr.precioSalida,det.cantidad,  iva ,det.idDetalleEncargo
            FROM productos pr,detallesencargos det, encargos en
            WHERE pr.idProducto=det.idProducto AND en.idEncargo=det.idEncargo
            AND en.idEncargo='" +idEncargo+@"'
            ORDER BY pr.nombreProducto;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable ProductosVenta(String idDocumento)
        {

            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT pr.idProducto, pr.nombreProducto,det.cantidadSalida,det.subTotal ,det.ivaCalculado,det.idDetalleDocumento,det.impuesto
            FROM productos pr,detalledocumentos det, documentos d
            WHERE pr.idProducto=det.idProducto AND d.idDocumento=det.idDocumento
            AND d.idDocumento='" + idDocumento+@"'
            ORDER BY pr.nombreProducto;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable PERMISOS_DE_USUARIOS(String pUsuario)
        {                       
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT a.idOpcion, a.opcion 
            from opciones a, permisos b, usuarios c
            where a.idOpcion=b.idOpcion and b.idRol=c.idRol and c.usuario='" + pUsuario + @"';");                      

            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable TODOS_LOS_ENCARGOS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT en.idEncargo, en.descripcion, en.fechaEncargo,
            en.fechaEntrega, en.lugarEntrega, en.idUsuario, en.idTitular,
            concat(ti.nombres,' ',ti.apellidos) as 'Cliente',
            usu.usuario
            from encargos en, titulares ti, usuarios usu
            where en.idTitular=ti.idTitular and usu.idUsuario=en.idUsuario
            Order by fechaEntrega,fechaEncargo,idEncargo desc;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable TODOS_LAS_VENTAS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT d.idDocumento, d.numeroDocumento, d.fecha, d.transaccion,
            d.formadePago, d.tipo, d.idTitular, d.total,
            concat(ti.nombres,' ',ti.apellidos) as 'Cliente'
            from documentos d, titulares ti
            where d.idTitular=ti.idTitular and d.transaccion='Venta'
            Order by d.idDocumento desc;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }
        public static DataTable TODOS_LAS_COMPRAS()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT d.idDocumento, d.numeroDocumento, d.fecha, d.transaccion,
            d.formadePago, d.tipo, d.idTitular, d.total,
            concat(ti.nombres,' ',ti.apellidos) as 'Cliente'
            from documentos d, titulares ti
            where d.idTitular=ti.idTitular and d.transaccion='Compra'
            Order by d.idDocumento desc;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable TODOS_LOS_TITULARES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idTitular, tipo, nombres, apellidos, 
             direccion, telefono, dui from titulares
             Order by apellidos, nombres;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable TODOS_LOS_CLIENTES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idTitular, nombres, apellidos, direccion, telefono, dui 
                               FROM titulares WHERE tipo='Cliente'
                               Order by apellidos, nombres;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }


        public static DataTable TODOS_LOS_ROLES()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT idRol, nombreRol from roles
             Order by nombreRol ASC");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }
        public static DataTable PERMISOS_DEUNROL(String idROL)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT  
            if((SELECT p.idPermiso FROM permisos p where idRol='"+idROL+@"' and p.idOpcion=a.idOpcion) is null,0,1) as 'Asignado',
            ifnull((SELECT p.idPermiso FROM permisos p where idRol='" + idROL + @"' and p.idOpcion=a.idOpcion),'') as 'idAsignacion', 
            a.idOpcion, a.opcion, a.clasificacion 
            FROM opciones a;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static String NuevoNumeroDocumento()
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select numeroDocumento from documentos order by numeroDocumento desc limit 0,1;");

            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }
            int valor = Convert.ToInt32( Resultado.Rows[0][0].ToString())+1;
            while (NuevoNumeroDocumentoYE(valor))
            {
                valor=valor+1;
            }
            return valor+"";
        }

        private static Boolean NuevoNumeroDocumentoYE(int Numero)
        {
            DataTable Resultado = new DataTable();
            Boolean valor = false;
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"select numeroDocumento from documentos WHERE numeroDocumento="+ Numero + ";");

            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }
            
            if (Resultado.Rows.Count > 0 ){
                valor = true;
            }

            return valor;
        }

        public static String IDDocuentoNumero(String NDocumento)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();
            Sentencia.Append(@"Select idDocumento from documentos Where numeroDocumento="+ NDocumento+ ";");

            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }
            return  Resultado.Rows[0][0].ToString();
        }

        public static DataTable ReporteEncargoCliente(String idEncargo)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT en.idEncargo, en.descripcion as 'DescripcionEncargo', pr.nombreProducto, den.descripcion as 'DescripcionProducto', nombreFoto,
            den.precio,den.cantidad,(den.precio*den.cantidad) as 'SubTotal',
            ifnull((SELECT anticipo FROM databaseproyectofinal.anticipos an WHERE an.idEncargo=en.idEncargo),'0.00') as Anticipo,
            (SELECT SUM(x.precio*x.cantidad) FROM databaseproyectofinal.detallesencargos x WHERE x.idEncargo=en.idEncargo)as Total,
            ((SELECT SUM(x.precio*x.cantidad) FROM databaseproyectofinal.detallesencargos x WHERE x.idEncargo=en.idEncargo)-
            (ifnull((SELECT anticipo FROM databaseproyectofinal.anticipos an WHERE an.idEncargo=en.idEncargo),'0.00'))) as 'Pendiente',
            fechaEncargo, fechaEntrega, lugarEntrega, us.idUsuario, ti.idTitular,
            us.usuario,CONCAT(ti.nombres,' ',ti.apellidos) as Cliente
            FROM databaseproyectofinal.detallesencargos den,  databaseproyectofinal.encargos en,databaseproyectofinal.productos pr,
            databaseproyectofinal.usuarios us, databaseproyectofinal.titulares ti
            WHERE den.idEncargo=en.idEncargo and pr.idProducto=den.idProducto and us.idUsuario=en.idUsuario and ti.idTitular=en.idTitular 
            AND en.idEncargo="+idEncargo+" ORDER BY pr.nombreProducto;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

        public static DataTable ReporteVenta(String idDocumento)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"select a.idDocumento, b.numeroDocumento, b.transaccion,b.tipo, concat(c.nombres, ' ', c.apellidos)
            as Cliente, p.nombreProducto, a.cantidadSalida, a.precio, a.subTotal, a.ivaCalculado,
            (select SUM(subTotal) from databaseproyectofinal. detalledocumentos x where x.idDetalleDocumento=b.idDocumento) as Total,
            b.formadePago, b.fecha
            from detalledocumentos a, documentos b, titulares c, productos p
            where a.idDocumento= b.idDocumento and c.idTitular=b.idTitular and p.idProducto= a.idProducto AND b.idDocumento=" + idDocumento + " order by p.nombreProducto;");


            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

    }
}
