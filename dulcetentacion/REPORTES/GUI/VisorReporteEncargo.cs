﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace REPORTES.GUI
{
    public partial class VisorReporteEncargo : Form
    {
        String _idEncargo;
        public VisorReporteEncargo()
        {
            InitializeComponent();
        }

        public string IdEncargo { get => _idEncargo; set => _idEncargo = value; }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void VisorReporteEncargo_Load(object sender, EventArgs e)
        {
            try
            {
                REP.ReporteEncargoCliente oreporte = new REP.ReporteEncargoCliente();
                oreporte.SetDataSource(CacheManager.SystemCache.ReporteEncargoCliente(IdEncargo));
                crystalReportViewer1.ReportSource = oreporte;
                // MessageBox.Show(seccion);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
