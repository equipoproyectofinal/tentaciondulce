﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Permisos
    {
        String _idPermiso;

        public String IdPermiso
        {
            get { return _idPermiso; }
            set { _idPermiso = value; }
        }
        String _idOpcion;

        public String IdOpcion
        {
            get { return _idOpcion; }
            set { _idOpcion = value; }
        }
        String _idRol;

        public String IdRol
        {
            get { return _idRol; }
            set { _idRol = value; }
        }

        public static Boolean AsignarPermiso(String idOpcion, String idRol)
        {
            Boolean Guardado = false;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            String Sentencia = "INSERT INTO permisos (idOpcion, idRol) VALUES ('" + idOpcion + "', '" + idRol + "');";
            try
            {
                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            
            }
            return Guardado;
            }

        public static Boolean RevocarPermiso(String idPermiso)
        {
            Boolean Eliminado= false;
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            String Sentencia = "DELETE FROM permisos WHERE idPermiso='"+idPermiso+"';";
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Eliminado = true;

                }
                else
                {
                    Eliminado = false;
                }
            }
            catch
            {
                Eliminado = false;

            }
            return Eliminado;
        }

    }

    

}
