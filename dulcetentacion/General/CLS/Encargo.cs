﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Encargo
    {
        private String _idEncargo;

        public String IdEncargo
        {
            get { return _idEncargo; }
            set { _idEncargo = value; }
        }
        private String _descripcion;

        public String Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        private String _fechaEncargo;

        public String FechaEncargo
        {
            get { return _fechaEncargo; }
            set { _fechaEncargo = value; }
        }
        private String _fechaEntrega;

        public String FechaEntrega
        {
            get { return _fechaEntrega; }
            set { _fechaEntrega = value; }
        }
        private String _lugarEntrega;

        public String LugarEntrega
        {
            get { return _lugarEntrega; }
            set { _lugarEntrega = value; }
        }
        private String _idUsuario;

        public String IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }
        private String _idTitular;

        public String IdTitular
        {
            get { return _idTitular; }
            set { _idTitular = value; }
        }



        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into encargos(descripcion, fechaEncargo, fechaEntrega, lugarEntrega, idUsuario, idTitular) values(");
            Sentencia.Append("'" + Descripcion + "',");
            Sentencia.Append("'" + FechaEncargo + "',");
            Sentencia.Append("'" + FechaEntrega + "',");
            Sentencia.Append("'" + LugarEntrega + "',");
            Sentencia.Append("'" + IdUsuario + "',");
            Sentencia.Append("'" + IdTitular + "');");
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();

            
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE encargos SET ");
            Sentencia.Append("descripcion='" + Descripcion + "',");
            Sentencia.Append("fechaEncargo='" + FechaEncargo + "',");
            Sentencia.Append("fechaEntrega='" + FechaEntrega + "',");
            Sentencia.Append("lugarEntrega='" + LugarEntrega + "',");
            Sentencia.Append("idUsuario='" + IdUsuario + "',");
            Sentencia.Append("idTitular='" + IdTitular + "' WHERE  idEncargo='" + IdEncargo +"';");

            

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM encargos ");
            Sentencia.Append("WHERE idEncargo='" + IdEncargo + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public  DataTable ProductosEncargo()
        {

            DataTable Resultado = new DataTable();
            StringBuilder Sentencia = new StringBuilder();
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia.Append(@"SELECT pr.idProducto, pr.nombreProducto,det.cantidad as 'Cantidad', (det.precio*det.cantidad) as 'SubTotal', pr.precioSalida,
            round((det.precio*det.cantidad)*0.13 ,2) as 'ivaCalculado','exento'
            FROM productos pr, detallesencargos det
            WHERE pr.idProducto=det.idProducto and idEncargo='"+ IdEncargo+ @"'
            ORDER BY pr.nombreProducto;");
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }

            return Resultado;
        }

    }
}
