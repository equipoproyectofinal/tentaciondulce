﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Documentos
    {
        private String _idDocumento;
        private String _numeroDocumento;
        private String _fecha;
        private String _transaccion;
        private String _formaPago;
        private String _idTitular;
        private String _tipo;
        private Double _total;

        public string IdDocumento { get => _idDocumento; set => _idDocumento = value; }
        public string NumeroDocumento { get => _numeroDocumento; set => _numeroDocumento = value; }
        public string Fecha { get => _fecha; set => _fecha = value; }
        public string Transaccion { get => _transaccion; set => _transaccion = value; }
        public string FormaPago { get => _formaPago; set => _formaPago = value; }
        public string IdTitular { get => _idTitular; set => _idTitular = value; }
        public string Tipo { get => _tipo; set => _tipo = value; }
        public double Total { get => _total; set => _total = value; }

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into documentos(numeroDocumento, fecha, transaccion, formadePago, idTitular, tipo, total) values(");
            Sentencia.Append("'" + NumeroDocumento + "',");
            Sentencia.Append("'" + Fecha + "',");
            Sentencia.Append("'" + Transaccion + "',");
            Sentencia.Append("'" + FormaPago + "',");
            Sentencia.Append("'" + IdTitular + "',");
            Sentencia.Append("'" + Tipo + "',");
            Sentencia.Append("'" + Total + "');");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();       

            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE documentos SET ");
            Sentencia.Append("numeroDocumento='" + NumeroDocumento + "',");
            Sentencia.Append("fecha='" + Fecha + "',");
            Sentencia.Append("transaccion='" + Transaccion + "',");
            Sentencia.Append("formadePago='" + FormaPago + "',");
            Sentencia.Append("idTitular='" + IdTitular + "',");
            Sentencia.Append("tipo='" + Tipo + "',");
            Sentencia.Append("total='" + Total + "' WHERE  idDocumento='" + IdDocumento + "';");



            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM documentos ");
            Sentencia.Append("WHERE idDocumento='" + IdDocumento + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }



    }
}
