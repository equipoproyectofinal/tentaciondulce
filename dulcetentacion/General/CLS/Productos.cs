﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Productos
    {
        private String _idProducto;

        public String IdProducto
        {
            get { return _idProducto; }
            set { _idProducto = value; }
        }
        private String _nombreProducto;

        public String NombreProducto
        {
            get { return _nombreProducto; }
            set { _nombreProducto = value; }
        }
        private Double _precioEntrada;

        public Double PrecioEntrada
        {
            get { return _precioEntrada; }
            set { _precioEntrada = value; }
        }
        private Double _precioSalida;

        public Double PrecioSalida
        {
            get { return _precioSalida; }
            set { _precioSalida = value; }
        }
        private String _tipoProducto;

        public String TipoProducto
        {
            get { return _tipoProducto; }
            set { _tipoProducto = value; }
        }
        private Double _iva;

        public Double Iva
        {
            get { return _iva; }
            set { _iva = value; }
        }


        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into productos(nombreProducto, precioEntrada, precioSalida, tipoProducto, iva) values(");
            Sentencia.Append("'" + NombreProducto + "',");
            Sentencia.Append("'" + PrecioEntrada + "',");
            Sentencia.Append("'" + PrecioSalida + "',");
            Sentencia.Append("'" + TipoProducto + "',");
            Sentencia.Append("'" + Iva + "');");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();


            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE productos SET ");
            Sentencia.Append("nombreProducto='" + NombreProducto + "',");
            Sentencia.Append("precioEntrada='" + PrecioEntrada + "',");
            Sentencia.Append("precioSalida='" + PrecioSalida + "',");
            Sentencia.Append("tipoProducto='" + TipoProducto + "',");
            Sentencia.Append("iva='" + Iva + "' WHERE idProducto='" + IdProducto + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM productos ");
            Sentencia.Append("WHERE idProducto='" + IdProducto + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean GuardarDetalle(String idEncargo,String Cantidad,String Descrip, String NomFot)
        {
            Boolean Guardado = false;
            String Sentencia = "INSERT INTO detallesencargos(idProducto,idEncargo,cantidad,descripcion,nombreFoto,precio)" +
                              "VALUES ('" + IdProducto + "', '" + idEncargo + "', '" + Cantidad + "', '" + Descrip + "', '"+NomFot+"', '"+_precioSalida+"');";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean GuardarDetalleVentas(String idDocumento, String Cantidad, String impuesto, String subtotal, String ivacalculado)
        {
            Boolean Guardado = false;
            String Sentencia = "INSERT INTO detalledocumentos(`idDocumento`, `idProducto`, `cantidadEntrada`, `cantidadSalida`, `precio`, `subTotal`, `ivaCalculado`, `impuesto`)"+
                                "VALUES('"+idDocumento+"', '"+IdProducto+"', 0, '"+Cantidad+"', '"+PrecioSalida+"', '"+subtotal+"', '"+ivacalculado+"', '"+impuesto+"'); ";
            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
    }
}
