﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Empleado
    {
        private String _idEmpleado;

        public String IdEmpleado
        {
            get { return _idEmpleado; }
            set { _idEmpleado = value; }
        }
        private String _nombres;

        public String Nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }
        private String _apellidos;

        public String Apellidos
        {
            get { return _apellidos; }
            set { _apellidos = value; }
        }
        private String _dui;

        public String Dui
        {
            get { return _dui; }
            set { _dui = value; }
        }
        private String _telefono;

        public String Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        private String _direccion;

        public String Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        private String _fechaNacimiento;

        public String FechaNacimiento
        {
            get { return _fechaNacimiento; }
            set { _fechaNacimiento = value; }
        }
        private String _nit;

        public String Nit
        {
            get { return _nit; }
            set { _nit = value; }
        }



        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into empleados(nombres, apellidos, dui, direccion, telefono, fechaNacimiento, nit) values(");
            Sentencia.Append("'" + Nombres + "',");
            Sentencia.Append("'" + Apellidos + "',");
            Sentencia.Append("'" + Dui + "',");
            Sentencia.Append("'" + Direccion + "',");
            Sentencia.Append("'" + Telefono + "',");
            Sentencia.Append("'" + FechaNacimiento + "',");
            Sentencia.Append("'" + Nit + "');");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();


            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE empleados SET ");
            Sentencia.Append("nombres='" + Nombres + "',");
            Sentencia.Append("apellidos='" + Apellidos + "',");            
            Sentencia.Append("dui='" + Dui + "',");
            Sentencia.Append("direccion='" + Direccion + "',");
            Sentencia.Append("telefono='" + Telefono + "',");
            Sentencia.Append("fechaNacimiento='" + FechaNacimiento + "',");
            Sentencia.Append("nit='" + Nit + "' WHERE idEmpleado='" + IdEmpleado + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM empleados ");
            Sentencia.Append("WHERE idEmpleado='" + IdEmpleado + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }



    }

}
