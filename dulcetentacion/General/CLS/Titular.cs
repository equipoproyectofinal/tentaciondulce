﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Titular
    {
        private String _idTitular;

        public String IdTitular
        {
            get { return _idTitular; }
            set { _idTitular = value; }
        }
        private String _tipo;

        public String Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }
        private String _nombres;

        public String Nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }
        private String _apellidos;

        public String Apellidos
        {
            get { return _apellidos; }
            set { _apellidos = value; }
        }
        private String _direccion;

        public String Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        private String _telefono;

        public String Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        private String _dui;

        public String Dui
        {
            get { return _dui; }
            set { _dui = value; }
        }

        

        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into titulares(tipo, nombres, apellidos, direccion, telefono, dui) values(");
            Sentencia.Append("'" + Tipo + "',");
            Sentencia.Append("'" + Nombres + "',");
            Sentencia.Append("'" + Apellidos + "',");            
            Sentencia.Append("'" + Direccion + "',");
            Sentencia.Append("'" + Telefono + "',");
            Sentencia.Append("'" + Dui + "');");       
            

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();


            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE titulares SET ");
            Sentencia.Append("tipo='" + Tipo + "',");
            Sentencia.Append("nombres='" + Nombres + "',");
            Sentencia.Append("apellidos='" + Apellidos + "',");           
            Sentencia.Append("direccion='" + Direccion + "',");
            Sentencia.Append("telefono='" + Telefono + "',");
            Sentencia.Append("dui='" + Dui + "' WHERE idTitular='" + IdTitular + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }
    }
}
