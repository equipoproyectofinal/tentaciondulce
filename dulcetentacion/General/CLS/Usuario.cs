﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.CLS
{
    class Usuario
    {
        String _idUsuario;

        public String IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }
        String _nombreUsuario;

        public String NombreUsuario
        {
            get { return _nombreUsuario; }
            set { _nombreUsuario = value; }
        }
        String _Credencial;

        public String Credencial
        {
            get { return _Credencial; }
            set { _Credencial = value; }
        }
        String _idEmpleado;

        public String IdEmpleado
        {
            get { return _idEmpleado; }
            set { _idEmpleado = value; }
        }
        String _idRol;

        public String IdRol
        {
            get { return _idRol; }
            set { _idRol = value; }
        }
        String _Estado;

        public String Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }



        public Boolean Guardar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("insert into usuarios(usuario, Credencial, idEmpleado, idRol, Estado) values(");
            Sentencia.Append("'" + NombreUsuario + "', sha1(");
            Sentencia.Append("'" + Credencial + "'),");
            Sentencia.Append("'" + IdEmpleado + "',");
            Sentencia.Append("'" + IdRol + "',");
            Sentencia.Append("'" + Estado + "');");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();


            try
            {
                if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }
            return Guardado;
        }
        public Boolean Actualizar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("UPDATE usuarios SET ");
            Sentencia.Append("usuario='" + NombreUsuario + "',");
            Sentencia.Append("Credencial='" + Credencial + "',");
            Sentencia.Append("idEmpleado='" + IdEmpleado + "',");
            Sentencia.Append("idRol='" + IdRol + "',");
            Sentencia.Append("Estado='" + Estado + "' WHERE idUsuario='" + IdUsuario + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;

                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }

        public Boolean Eliminar()
        {
            Boolean Guardado = false;
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM usuarios ");
            Sentencia.Append("WHERE idUsuario='" + IdUsuario + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    Guardado = true;
                }
                else
                {
                    Guardado = false;
                }
            }
            catch
            {
                Guardado = false;
            }

            return Guardado;
        }




    }
}
