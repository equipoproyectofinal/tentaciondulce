﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using General.CLS;
using REPORTES.GUI;

namespace General.GUI
{
    public partial class DetalleVentas : Form
    {
        Documentos oDocumentos = new Documentos();
        internal Documentos ODocumento
        {
            get { return oDocumentos; }
            set { oDocumentos = value; }
        }
        BindingSource _Productos = new BindingSource();
        public DetalleVentas()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            CargarDatos();
        }
      
        private void CargarDatos()
        {
            try
            {
                _Productos.DataSource = CacheManager.SystemCache.ProductosVenta(oDocumentos.IdDocumento);
            }
            catch
            {

            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Productos;
            CalcularTotal();
        }       

        private void lblAnticipo_Click(object sender, EventArgs e)
        {

        }

        private void txbAnticipo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }                    

        public void CalcularTotal()
        {
            double total = 0;
            double aux = 0;
            double impu = 0;
            //MessageBox.Show(dtgvDatos.Rows[0].Cells[2].Value.ToString());
            for (int i = 0; i < dtgvDatos.Rows.Count; i++)
            {
                if (dtgvDatos.Rows[i].Cells[5].Value.ToString()=="gravado")
                {
                    impu = Convert.ToDouble(dtgvDatos.Rows[i].Cells[4].Value.ToString());
                   // MessageBox.Show(dtgvDatos.Rows[i].Cells[4].Value.ToString());
                }
                aux = Convert.ToDouble(dtgvDatos.Rows[i].Cells[2].Value.ToString())+ impu;
                total = total + aux;
            }
            lblTotal.Text = "$ " + total;
        }
           
       

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            BuscarProducto vent = new BuscarProducto();

            vent.IdDocumento = oDocumentos.IdDocumento;
            vent.ShowDialog();
            CargarDatos();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM detalledocumentos WHERE idDetalleDocumento='" + dtgvDatos.CurrentRow.Cells["idDetalleDocumento"].Value.ToString() + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    MessageBox.Show("Borrado con Exito");
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }

            CargarDatos();
        }

        private void DetalleVentas_Load(object sender, EventArgs e)
        {
            CargarDatos();

            txbIdTitular.Text = oDocumentos.IdTitular;
            txbNumDoc.Text = oDocumentos.NumeroDocumento;

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void LblTitular_Click(object sender, EventArgs e)
        {

        }

        private void DtgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void TxbNumDoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbIdTitular_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private void LblTotal_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            VisorReporteVenta nueva = new VisorReporteVenta();
            nueva.IdDocumento = oDocumentos.IdDocumento;
            nueva.ShowDialog();
        }
    }
}
