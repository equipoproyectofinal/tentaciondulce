﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionProductos : Form
    {
        BindingSource _Productos = new BindingSource();

        public GestionProductos()
        {
            InitializeComponent();
        }

        private void CargarDatos()
        {
            try
            {
                _Productos.DataSource = CacheManager.SystemCache.TODOS_LOS_PRODUCTOS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }
        private void FiltrarLocalmente()
        {
            try
            {

                if (txbFiltro.TextLength > 0)
                {
                    _Productos.Filter = "nombreProducto like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Productos.RemoveFilter();
            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Productos;
            lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros Encontrados ";
        }

        private void dtgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
          
            

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Productos oProducto = new CLS.Productos();
                oProducto.IdProducto = dtgvDatos.CurrentRow.Cells["idProducto"].Value.ToString();
                if (oProducto.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnEditar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea editar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionProductos f = new EdicionProductos();

                f.txbidProducto.Text = dtgvDatos.CurrentRow.Cells["idProducto"].Value.ToString();
                f.txbNombreProducto.Text = dtgvDatos.CurrentRow.Cells["nombreProducto"].Value.ToString();
                f.txbPrecioEntrada.Text = dtgvDatos.CurrentRow.Cells["precioEntrada"].Value.ToString();
                f.txbPrecioSalida.Text = dtgvDatos.CurrentRow.Cells["precioSalida"].Value.ToString();
                f.cbxTipoProducto.Text = dtgvDatos.CurrentRow.Cells["tipoProducto"].Value.ToString();
                f.txbIva.Text = dtgvDatos.CurrentRow.Cells["iva"].Value.ToString();

                f.ShowDialog();
                CargarDatos();

            }
        }

        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            EdicionProductos nuevo = new EdicionProductos();
            nuevo.ShowDialog();
            CargarDatos();
        }

        private void GestionProductos_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void LblRegistros_Click(object sender, EventArgs e)
        {

        }

        private void StatusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
