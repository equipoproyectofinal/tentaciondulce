﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionDocumentos : Form
    {
        BindingSource _Titulares = new BindingSource();
        public GestionDocumentos()
        {
            InitializeComponent();
        }
        private void CargarDatos()
        {
            try
            {
                _Titulares.DataSource = CacheManager.SystemCache.TODOS_LOS_ROLES();
             
            }
            catch
            {

            }
        }


        private void btnBuscarTitular_Click(object sender, EventArgs e)
        {
            //AbrirEnPanel(new General.GUI.GestionDocumentos());
            General.GUI.BuscarCliente f = new General.GUI.BuscarCliente();
            //Solo se puede poner esta cuando la opción IsMdiContainer está habilitada pero en este caso
            //no se puede porque ya está dentro de otro IsMdiContainer así q mejor que se abra aparte
            //f.MdiParent = this;
            f.Show();
            CargarDatos();
        }

        private void btnAgregarTitular_Click(object sender, EventArgs e)
        {
            General.GUI.EdicionTitulares nuevo = new General.GUI.EdicionTitulares();            
            CargarDatos();
            nuevo.Show();

        }

        private void btnEditarTitular_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea cambiar de Titular?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                BuscarCliente f = new BuscarCliente();
                f.Show();
                CargarDatos();

            }
        }

        private void btnBuscarArticulo_Click(object sender, EventArgs e)
        {
            
            General.GUI.GestionProductos f = new General.GUI.GestionProductos();            
            f.Show();
            CargarDatos();
        }
    }
}
