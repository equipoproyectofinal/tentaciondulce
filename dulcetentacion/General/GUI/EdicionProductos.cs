﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionProductos : Form
    {
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Productos oProducto = new CLS.Productos();
                oProducto.IdProducto = txbidProducto.Text;
                oProducto.NombreProducto = txbNombreProducto.Text;
                oProducto.PrecioEntrada = Convert.ToDouble(txbPrecioEntrada.Text);
                oProducto.PrecioSalida= Convert.ToDouble(txbPrecioSalida.Text);
                oProducto.TipoProducto = cbxTipoProducto.SelectedItem.ToString();
                oProducto.Iva = Convert.ToDouble(txbIva.Text);
                if (txbidProducto.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oProducto.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oProducto.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }

        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbNombreProducto.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbNombreProducto, "El campo no puede quedar vacio");
            }
            if (txbPrecioEntrada.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbPrecioEntrada, "El campo no puede quedar vacio");
            }
            //Verificar que el campo textlength en campo fecha da error, debe ir separado con punto
            if (txbPrecioSalida.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbPrecioSalida, "El campo no puede quedar vacio");
            }
            if (cbxTipoProducto.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxTipoProducto, "El campo no puede quedar vacio");
            }
            if (txbIva.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbIva, "El campo no puede quedar vacio");
            }

            return Verificado;
        }
        public EdicionProductos()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }
    }
}
