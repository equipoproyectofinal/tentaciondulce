﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class BuscarProductoTerminado : Form
    {
        BindingSource _Product = new BindingSource();
        CLS.Productos oProducto = new CLS.Productos();
        String idEncargo;

        public String IdEncargo
        {
            get { return idEncargo; }
            set { idEncargo = value; }
        }
        
        internal CLS.Productos OProducto
        {
            get { return oProducto; }
            set { oProducto = value; }
        }


        public BuscarProductoTerminado()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void CargarDatos()
        {
            try
            {
                _Product.DataSource = CacheManager.SystemCache.SOLO_PRODUCTOS_TERMINADOS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Product.Filter = "nombreProducto like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Product.RemoveFilter();
            }
            dtgvProductos.AutoGenerateColumns = false;
            dtgvProductos.DataSource = _Product;
        }

        private void BuscarProductoTerminado_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        public void ProductoSeleccionado()
        {
            oProducto.IdProducto = dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString();
            oProducto.NombreProducto = dtgvProductos.CurrentRow.Cells["nombreProducto"].Value.ToString();
            oProducto.PrecioSalida = double.Parse(dtgvProductos.CurrentRow.Cells["precioSalida"].Value.ToString());
            oProducto.Iva = float.Parse(dtgvProductos.CurrentRow.Cells["iva"].Value.ToString());
            
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ProductoSeleccionado();
            if (oProducto.GuardarDetalle(idEncargo, numericant.Value.ToString(), txbDescripcion.Text, txbnombre.Text))
            {
                Close();
            }
            else { 
            
            }
        }
    }
}
