﻿using General.CLS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionPermisos : Form
    {
         BindingSource _PERMISOS = new BindingSource();
        public GestionPermisos()
        {
            InitializeComponent();
        }

        private void GestionPermisos_Load(object sender, EventArgs e)
        {
            CargarRoles();
            CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());
        }

        private void CargarRoles(){
            
            try
            {
                cbbRoles.DataSource = CacheManager.SystemCache.TODOS_LOS_ROLES();
                cbbRoles.DisplayMember="nombreRol";
                cbbRoles.ValueMember="idRol";
                cbbRoles.SelectedIndex=0;
            }
            catch
            {

            }
        }

        private void CargarPermisosDelRol(String idRol)
        {
            try
            {
                _PERMISOS.DataSource = CacheManager.SystemCache.PERMISOS_DEUNROL(idRol);
                dgvPermisos.AutoGenerateColumns = false;
                dgvPermisos.DataSource = _PERMISOS;

            }
            catch
            {

            }
        }

        private void cbbRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());

            }

        private void dgvPermisos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==0){
                String Valor = dgvPermisos.CurrentRow.Cells["Asignado"].Value.ToString();
                //MessageBox.Show(Valor);
                if(Valor=="1"){
                    String VAL = null;
                    try
                    {
                        VAL = dgvPermisos.CurrentRow.Cells["idAsignacion"].Value.ToString();
                    }
                    catch
                    {
                        VAL = "0";
                    }
                    
                    if (Permisos.RevocarPermiso(VAL) == true)
                    {
                        MessageBox.Show("SE REVOCÓ CON EXITO");
                        CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());
                    }
                    else
                    {
                        MessageBox.Show("ERROR AL REVOCAR","Error");
                        CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());
                    }

                }
                if (Valor == "0") {
                    if (Permisos.AsignarPermiso(dgvPermisos.CurrentRow.Cells["idOpcion"].Value.ToString(),cbbRoles.SelectedValue.ToString()) == true)
                    {
                        MessageBox.Show("SE ASIGNO CON EXITO");
                        CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());
                    }
                    else
                    {
                        MessageBox.Show("ERROR DE ASIGNACION", "Error");
                        CargarPermisosDelRol(cbbRoles.SelectedValue.ToString());
                    }
                {

                }
            }
        }
        }
    
    }
}