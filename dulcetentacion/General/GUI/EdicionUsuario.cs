﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionUsuario : Form
    {
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Usuario oUsuario = new CLS.Usuario();
                oUsuario.IdUsuario = txbIdUsuario.Text;
                oUsuario.NombreUsuario = txbUsuario.Text;
                oUsuario.Credencial = txbCredencial.Text;
                oUsuario.IdEmpleado = txbIdEmpleado.Text;
                oUsuario.IdRol = txbIdRol.Text;
                oUsuario.Estado = cbxEstado.Text;
                if (txbIdUsuario.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oUsuario.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oUsuario.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }

        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbUsuario.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbUsuario, "El campo no puede quedar vacio");
            }
            if (txbCredencial.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbCredencial, "El campo no puede quedar vacio");
            }
            //Verificar que el campo textlength en campo fecha da error, debe ir separado con punto
            if (txbIdEmpleado.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbIdEmpleado, "El campo no puede quedar vacio");
            }
            if (txbIdRol.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbIdRol, "El campo no puede quedar vacio");
            }
            if (cbxEstado.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxEstado, "El campo no puede quedar vacio");
            }

            return Verificado;
        }
        public EdicionUsuario()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }
    }
}
