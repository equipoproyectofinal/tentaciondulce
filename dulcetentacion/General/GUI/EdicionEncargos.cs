﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionEncargos : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        CLS.Titular oTitular = new CLS.Titular();
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Encargo oEncargo = new CLS.Encargo();
                oEncargo.IdEncargo = txbIdEncargo.Text;
                oEncargo.Descripcion = txbDescripcion.Text;
                oEncargo.FechaEncargo = dtpFechaEncargo.Text;
                oEncargo.FechaEntrega = dtpFechaEntrega.Text;
                oEncargo.LugarEntrega = txbLugarEntrega.Text;
                oEncargo.IdUsuario = txbIdUsuario.Text;
                oEncargo.IdTitular = txbIdTitular.Text;
                
                if (txbIdEncargo.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oEncargo.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Close();
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oEncargo.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }


        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbDescripcion.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbDescripcion, "El campo no puede quedar vacio");
            }
            if (dtpFechaEncargo.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpFechaEncargo, "El campo no puede quedar vacio");
            }
            //Verificar que el campo textlength en campo fecha da error, debe ir separado con punto
            if (dtpFechaEntrega.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpFechaEntrega, "El campo no puede quedar vacio");
            }
            if (txbLugarEntrega.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbLugarEntrega, "El campo no puede quedar vacio");
            }
            if (txbIdUsuario.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbIdUsuario, "El campo no puede quedar vacio");
            }
            if (txbIdTitular.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbIdTitular, "El campo no puede quedar vacio");
            }
            

            return Verificado;
        }
        public EdicionEncargos()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();
        }

        private void EdicionEncargos_Load(object sender, EventArgs e)
        {
            lblNombreUser.Text = _SESION.OUsuario.NombreUsuario;
            txbIdUsuario.Text = _SESION.OUsuario.IDUsuario;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblNombreUser_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            BuscarCliente nuevo = new BuscarCliente();
            nuevo.ShowDialog();
            this.oTitular = nuevo.OCliente;
            lblTitular.Text = oTitular.Nombres + " " + oTitular.Apellidos;
            txbIdTitular.Text = oTitular.IdTitular;
            //AbrirFormulario<General.GUI.EdicionEncargos>();
        }

        private void btnNuevoTitular_Click(object sender, EventArgs e)
        {
            General.GUI.EdicionTitulares nuevo = new General.GUI.EdicionTitulares();
            nuevo.Show();
        }
    }
}






