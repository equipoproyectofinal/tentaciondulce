﻿using General.CLS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class DetallesEncargo : Form
    {
        Encargo oEncargo = new Encargo();
        internal Encargo OEncargo
        {
            get { return oEncargo; }
            set { oEncargo = value; }
        }
        BindingSource _Productos = new BindingSource();
        public DetallesEncargo()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void DetallesEncargo_Load(object sender, EventArgs e)
        {
            CargarDatos();
            txbIdUsuario.Text = oEncargo.IdUsuario;
            txbIdTitular.Text = oEncargo.IdTitular;
            txbIdEncargo.Text = oEncargo.IdEncargo;
            verificarAnticipoinicio();
        }

        private void CargarDatos()
        {
            try
            {
                _Productos.DataSource = CacheManager.SystemCache.ProductosEncargo(oEncargo.IdEncargo);
            }
            catch
            {

            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Productos;
            CalcularTotal();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BuscarProductoTerminado vent = new BuscarProductoTerminado();
            vent.IdEncargo = oEncargo.IdEncargo;
            vent.ShowDialog();
            CargarDatos();
            
        }

        private void lblAnticipo_Click(object sender, EventArgs e)
        {

        }

        private void txbAnticipo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbAnticipo.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbAnticipo, "El campo no puede quedar vacio");
            }
            return Verificado;
         }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            if(Verificacion()){
            if (verificarAnticipo() == true )
            {
                string idDate = DateTime.Now.ToString("yyyy-MM-dd");
                String Sentencia = "INSERT INTO anticipos(anticipo,fecha,idEncargo,idUsuario) VALUES ('" + txbAnticipo.Text + "', '" + idDate + "', '"+oEncargo.IdEncargo+"', '"+oEncargo.IdUsuario+"');";
                
                DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
                MessageBox.Show(Sentencia);
                try
                {
                    if (oOperacion.Insertar(Sentencia.ToString()) > 0)
                    {
                        MessageBox.Show("Exito1");
                    }
                    else
                    {
                        MessageBox.Show("Error1");
                    }
                }
                catch
                {
                    MessageBox.Show("Error1");
                }
            }
            else
            {

                String Sentencia = "UPDATE anticipos SET anticipo='"+txbAnticipo.Text+"' WHERE idEncargo='"+oEncargo.IdEncargo+"';";
                MessageBox.Show(Sentencia);
                DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
                try
                {
                    if (oOperacion.Actualizar(Sentencia.ToString()) > 0)
                    {
                        MessageBox.Show("Exito2");
                    }
                    else
                    {
                        MessageBox.Show("Error2");
                    }
                }
                catch
                {
                    MessageBox.Show("Error2");
                }

            }
               }
        }

        private Boolean verificarAnticipo()
        {
            DataTable Resultado = new DataTable();
            String Sentencia;
            Boolean f = false;
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia = "SELECT anticipo FROM anticipos WHERE idEncargo="+oEncargo.IdEncargo+";";
            //MessageBox.Show(Sentencia);
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }
            int nrows = Resultado.Rows.Count;
            if (nrows == 0){

                f=true;
            }
            if(nrows != 0 )
            {

                f= false;
                
            }
            return f;
        }


        private void verificarAnticipoinicio()
        {
            DataTable Resultado = new DataTable();
            String Sentencia;
            Boolean f = false;
            DBManager.CLS.DBOperacion oConsulta = new DBManager.CLS.DBOperacion();

            Sentencia = "SELECT anticipo FROM anticipos WHERE idEncargo=" + oEncargo.IdEncargo + ";";
            //MessageBox.Show(Sentencia);
            try
            {
                Resultado = oConsulta.Consultar(Sentencia.ToString());
            }
            catch
            {
                Resultado = new DataTable();

            }
            int nrows = Resultado.Rows.Count;
            if (nrows == 0)
            {
                txbAnticipo.Text = "0.00";
            }
            if (nrows != 0)
            {
                txbAnticipo.Text = Resultado.Rows[0][0].ToString();
                f = false;

            }
            
        }


        public void CalcularTotal()
        {
            double total=0;
            double aux = 0;
            //MessageBox.Show(dtgvDatos.Rows[0].Cells[2].Value.ToString());
            for (int i = 0; i < dtgvDatos.Rows.Count;i++ ) {
                aux = Convert.ToDouble(dtgvDatos.Rows[i].Cells[2].Value.ToString());
                total = total + aux * Convert.ToInt32(dtgvDatos.Rows[i].Cells[3].Value.ToString());
            }
            lblTotal.Text ="$ "+ total;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            StringBuilder Sentencia = new StringBuilder();
            Sentencia.Append("DELETE FROM detallesencargos WHERE idDetalleEncargo='" + dtgvDatos.CurrentRow.Cells["idDetalleEncargo"].Value.ToString() + "';");

            DBManager.CLS.DBOperacion oOperacion = new DBManager.CLS.DBOperacion();
            try
            {

                if (oOperacion.Eliminar(Sentencia.ToString()) > 0)
                {
                    MessageBox.Show("Borrado con Exito");
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }

            CargarDatos();
        }

        private void lblTotal_Click(object sender, EventArgs e)
        {

        }
    }
}
