﻿using General.CLS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionCompras : Form
    {
        BindingSource _Documento = new BindingSource();
       
        private void CargarDatos()
        {
            try
            {
                _Documento.DataSource = CacheManager.SystemCache.TODOS_LAS_COMPRAS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Documento.Filter = "Cliente like '%" + txbFiltro.Text + "%'"; // OR fechaEncargo like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Documento.RemoveFilter();
            }
            dtgvCompras.AutoGenerateColumns = false;
            dtgvCompras.DataSource = _Documento;
            lblRegistros.Text = dtgvCompras.Rows.Count.ToString() + " Registros Encontrados";
        }               
            
       
                           

        public GestionCompras()
        {
            InitializeComponent();
            CargarDatos();
        }

        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            General.GUI.EdicionDocumento nuevo = new General.GUI.EdicionDocumento();
            nuevo.ShowDialog();
        }

        private void GestionCompras_Load(object sender, EventArgs e)
        {
         CargarDatos();
        }

        private void btnModificar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionDocumento f = new EdicionDocumento();

                f.txbIdDocumento.Text = dtgvCompras.CurrentRow.Cells["idDocumento"].Value.ToString();
                f.txbNumDocumento.Text = dtgvCompras.CurrentRow.Cells["numeroDocumento"].Value.ToString();
                f.dtpFecha.Text = dtgvCompras.CurrentRow.Cells["fecha"].Value.ToString();
                f.cbxTransaccion.Text = dtgvCompras.CurrentRow.Cells["transaccion"].Value.ToString();
                f.cbxFormaPago.Text = dtgvCompras.CurrentRow.Cells["formadePago"].Value.ToString();
                f.cbxTipo.Text = dtgvCompras.CurrentRow.Cells["tipo"].Value.ToString();
                f.txbTotal.Text = dtgvCompras.CurrentRow.Cells["total"].Value.ToString();
                f.lblTitular.Text = dtgvCompras.CurrentRow.Cells["Cliente"].Value.ToString();
                //f.btnBuscarCliente.Enabled = false;
                f.ShowDialog();
                CargarDatos();

            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Documentos oDocumento = new CLS.Documentos();
                oDocumento.IdDocumento = dtgvCompras.CurrentRow.Cells["idDocumento"].Value.ToString();
                if (oDocumento.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txbFiltro_TextChanged_1(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnBuscarDetalle_Click_1(object sender, EventArgs e)
        {
            Documentos Ob = new Documentos();
            Ob.IdDocumento = dtgvCompras.CurrentRow.Cells["idDocumento"].Value.ToString();
            Ob.IdTitular = dtgvCompras.CurrentRow.Cells["idTitular"].Value.ToString();
            DetalleCompras nuevo = new DetalleCompras();
            nuevo.ODocumento = Ob;
            nuevo.lblTitular.Text = dtgvCompras.CurrentRow.Cells["Cliente"].Value.ToString();
            nuevo.ShowDialog();
            CargarDatos();
        }

        private void ToolStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void StatusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
