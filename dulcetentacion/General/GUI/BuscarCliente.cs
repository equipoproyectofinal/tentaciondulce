﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class BuscarCliente : Form
    {
        BindingSource _Clientes = new BindingSource();
        CLS.Titular oCliente = new CLS.Titular();

        internal CLS.Titular OCliente
        {
            get { return oCliente; }
            set { oCliente = value; }
        }
        public BuscarCliente()
        {
            InitializeComponent();
        }
        private void CargarDatos()
        {
            try
            {
                _Clientes.DataSource = CacheManager.SystemCache.TODOS_LOS_CLIENTES();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Clientes.Filter = "nombres like '%" + txbFiltro.Text + "%' OR apellidos like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Clientes.RemoveFilter();
            }
            dtgvTitulares.AutoGenerateColumns = false;
            dtgvTitulares.DataSource = _Clientes;
            lblRegistros.Text = dtgvTitulares.Rows.Count.ToString() + "Registros Encontrados";
        }

        private void GestionTitulares_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ClienteSeleccionado();
        }

        public void ClienteSeleccionado(){
            oCliente.IdTitular = dtgvTitulares.CurrentRow.Cells["idTitular"].Value.ToString();
            oCliente.Nombres = dtgvTitulares.CurrentRow.Cells["nombres"].Value.ToString();
            oCliente.Apellidos = dtgvTitulares.CurrentRow.Cells["apellidos"].Value.ToString();
            Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
