﻿namespace General.GUI
{
    partial class GestionPermisos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbbRoles = new System.Windows.Forms.ComboBox();
            this.dgvPermisos = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Asignado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idOpcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.opcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clasificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idAsignacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPermisos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ROL";
            // 
            // cbbRoles
            // 
            this.cbbRoles.FormattingEnabled = true;
            this.cbbRoles.Location = new System.Drawing.Point(186, 70);
            this.cbbRoles.Name = "cbbRoles";
            this.cbbRoles.Size = new System.Drawing.Size(207, 21);
            this.cbbRoles.TabIndex = 1;
            this.cbbRoles.SelectedIndexChanged += new System.EventHandler(this.cbbRoles_SelectedIndexChanged);
            // 
            // dgvPermisos
            // 
            this.dgvPermisos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPermisos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPermisos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Asignado,
            this.idOpcion,
            this.opcion,
            this.clasificacion,
            this.idAsignacion});
            this.dgvPermisos.Location = new System.Drawing.Point(1, 116);
            this.dgvPermisos.Name = "dgvPermisos";
            this.dgvPermisos.RowHeadersVisible = false;
            this.dgvPermisos.Size = new System.Drawing.Size(546, 331);
            this.dgvPermisos.TabIndex = 2;
            this.dgvPermisos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPermisos_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(197, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "GESTION DE PERMISOS";
            // 
            // Asignado
            // 
            this.Asignado.DataPropertyName = "Asignado";
            this.Asignado.FalseValue = "0";
            this.Asignado.HeaderText = "Asignado";
            this.Asignado.Name = "Asignado";
            this.Asignado.ReadOnly = true;
            this.Asignado.TrueValue = "1";
            // 
            // idOpcion
            // 
            this.idOpcion.DataPropertyName = "idOpcion";
            this.idOpcion.HeaderText = "idOpcion";
            this.idOpcion.Name = "idOpcion";
            this.idOpcion.ReadOnly = true;
            // 
            // opcion
            // 
            this.opcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.opcion.DataPropertyName = "opcion";
            this.opcion.HeaderText = "Opcion";
            this.opcion.Name = "opcion";
            this.opcion.ReadOnly = true;
            // 
            // clasificacion
            // 
            this.clasificacion.DataPropertyName = "clasificacion";
            this.clasificacion.HeaderText = "Clasificacion";
            this.clasificacion.Name = "clasificacion";
            this.clasificacion.ReadOnly = true;
            // 
            // idAsignacion
            // 
            this.idAsignacion.DataPropertyName = "idAsignacion";
            this.idAsignacion.HeaderText = "idAsignacion";
            this.idAsignacion.Name = "idAsignacion";
            this.idAsignacion.ReadOnly = true;
            this.idAsignacion.Visible = false;
            // 
            // GestionPermisos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvPermisos);
            this.Controls.Add(this.cbbRoles);
            this.Controls.Add(this.label1);
            this.Name = "GestionPermisos";
            this.Text = "GestionPermisos";
            this.Load += new System.EventHandler(this.GestionPermisos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPermisos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbRoles;
        private System.Windows.Forms.DataGridView dgvPermisos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Asignado;
        private System.Windows.Forms.DataGridViewTextBoxColumn idOpcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn opcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn clasificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAsignacion;
    }
}