﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionUsuarios : Form
    {
        BindingSource _Usuarios = new BindingSource();
        public GestionUsuarios()
        {
            InitializeComponent();
        }
        private void CargarDatos()
        {
            try
            {
                _Usuarios.DataSource = CacheManager.SystemCache.TODOS_LOS_USUARIOS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }
        private void FiltrarLocalmente()
        {
            try
            {

                if (txbFiltro.TextLength > 0)
                {
                    _Usuarios.Filter = "Usuario like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Usuarios.RemoveFilter();
            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Usuarios;
            lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + "  Registros Encontrados ";
        }     
        
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Usuario oUsuario = new CLS.Usuario();
                oUsuario.IdUsuario = dtgvDatos.CurrentRow.Cells["idUsuario"].Value.ToString();
                if (oUsuario.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea editar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionUsuario f = new EdicionUsuario();

                f.txbIdUsuario.Text = dtgvDatos.CurrentRow.Cells["idUsuario"].Value.ToString();
                f.txbUsuario.Text = dtgvDatos.CurrentRow.Cells["usuario"].Value.ToString();
                f.txbIdEmpleado.Text = dtgvDatos.CurrentRow.Cells["idEmpleado"].Value.ToString();
                f.txbIdRol.Text = dtgvDatos.CurrentRow.Cells["idRol"].Value.ToString();
                f.cbxEstado.Text = dtgvDatos.CurrentRow.Cells["Estado"].Value.ToString();

                f.ShowDialog();
                CargarDatos();

            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionUsuario nuevo = new EdicionUsuario();
            nuevo.ShowDialog();
            CargarDatos();
        }

        private void dtgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GestionUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void StatusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void DtgvDatos_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {

        }
    }
}
