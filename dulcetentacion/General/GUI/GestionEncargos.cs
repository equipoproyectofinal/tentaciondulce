﻿using General.CLS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestionEncargos : Form
    {
        BindingSource _Encargo = new BindingSource();
        public GestionEncargos()
        {
            InitializeComponent();
        }

        private void CargarDatos()
        {
            try
            {
                _Encargo.DataSource = CacheManager.SystemCache.TODOS_LOS_ENCARGOS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Encargo.Filter = "Cliente like '%" + txbFiltro.Text + "%'"; // OR fechaEncargo like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Encargo.RemoveFilter();
            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Encargo;
            lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros Encontrados";
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionEncargos nuevo = new EdicionEncargos();
            nuevo.ShowDialog();
            //AbrirFormulario<General.GUI.EdicionEncargos>();
            CargarDatos();
        }

        private void GestionEncargos_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionEncargos f = new EdicionEncargos();

                f.txbIdEncargo.Text = dtgvDatos.CurrentRow.Cells["idEncargo"].Value.ToString();
                f.txbDescripcion.Text = dtgvDatos.CurrentRow.Cells["descripcion"].Value.ToString();
                f.dtpFechaEncargo.Text = dtgvDatos.CurrentRow.Cells["fechaEncargo"].Value.ToString();
                f.dtpFechaEntrega.Text = dtgvDatos.CurrentRow.Cells["fechaEntrega"].Value.ToString();
                f.txbLugarEntrega.Text = dtgvDatos.CurrentRow.Cells["lugarEntrega"].Value.ToString();
                f.txbIdUsuario.Text = dtgvDatos.CurrentRow.Cells["idUsuario"].Value.ToString();
                f.txbIdTitular.Text = dtgvDatos.CurrentRow.Cells["idTitular"].Value.ToString();
                f.lblTitular.Text = dtgvDatos.CurrentRow.Cells["Cliente"].Value.ToString();
                f.lblNombreUser.Text = dtgvDatos.CurrentRow.Cells["usuario"].Value.ToString();
                //f.btnBuscarCliente.Enabled = false;
                f.ShowDialog();
                CargarDatos();

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Encargo oEncargo = new CLS.Encargo();
                oEncargo.IdEncargo = dtgvDatos.CurrentRow.Cells["idEncargo"].Value.ToString();
                if (oEncargo.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txbFiltro_TextChanged_1(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            Encargo Ob = new Encargo();
            Ob.IdEncargo = dtgvDatos.CurrentRow.Cells["idEncargo"].Value.ToString();
            Ob.IdUsuario = dtgvDatos.CurrentRow.Cells["idUsuario"].Value.ToString();
            Ob.IdTitular = dtgvDatos.CurrentRow.Cells["idTitular"].Value.ToString();
            DetallesEncargo f = new DetallesEncargo();
            f.OEncargo = Ob;
            f.lblTitular.Text = dtgvDatos.CurrentRow.Cells["Cliente"].Value.ToString();
            f.lblNombreUser.Text = dtgvDatos.CurrentRow.Cells["usuario"].Value.ToString();
            f.ShowDialog();
            CargarDatos();
        }

        private void BtnEntregar_Click(object sender, EventArgs e)
        {
            EntregarEncargo mn = new EntregarEncargo();
            CLS.Encargo oEncargo = new CLS.Encargo();
            oEncargo.IdEncargo = dtgvDatos.CurrentRow.Cells["idEncargo"].Value.ToString();
            oEncargo.IdUsuario = dtgvDatos.CurrentRow.Cells["idUsuario"].Value.ToString();
            oEncargo.IdTitular = dtgvDatos.CurrentRow.Cells["idTitular"].Value.ToString();
            mn.lblTitular.Text = dtgvDatos.CurrentRow.Cells["Cliente"].Value.ToString();
            mn.OEncargo = oEncargo;
            mn.ShowDialog();
        }

        private void BtnTodos_Click(object sender, EventArgs e)
        {
            TodosEncargos vent = new TodosEncargos();
            vent.ShowDialog();
        }

        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
