﻿namespace General.GUI
{
    partial class EdicionDocumento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EdicionDocumento));
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txbIdDocumento = new System.Windows.Forms.TextBox();
            this.txbNumDocumento = new System.Windows.Forms.TextBox();
            this.txbTitular = new System.Windows.Forms.TextBox();
            this.txbTotal = new System.Windows.Forms.TextBox();
            this.cbxTransaccion = new System.Windows.Forms.ComboBox();
            this.cbxFormaPago = new System.Windows.Forms.ComboBox();
            this.cbxTipo = new System.Windows.Forms.ComboBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.Notificador = new System.Windows.Forms.ErrorProvider(this.components);
            this.btnBuscarCliente = new System.Windows.Forms.Button();
            this.btnNuevoTitular = new System.Windows.Forms.Button();
            this.lblTitular = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(536, 398);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(130, 51);
            this.btnGuardar.TabIndex = 0;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click_1);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(672, 398);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(130, 51);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "idDocumento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "numDocumento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(70, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fecha:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 272);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Transaccion:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(454, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Forma de Pago:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(219, 339);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "idTitular:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(441, 164);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "TipoDocumento:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(502, 229);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Total:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(270, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(413, 32);
            this.label9.TabIndex = 10;
            this.label9.Text = "AGREGAR UN DOCUMENTO";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txbIdDocumento
            // 
            this.txbIdDocumento.Location = new System.Drawing.Point(184, 78);
            this.txbIdDocumento.Name = "txbIdDocumento";
            this.txbIdDocumento.ReadOnly = true;
            this.txbIdDocumento.Size = new System.Drawing.Size(224, 26);
            this.txbIdDocumento.TabIndex = 11;
            // 
            // txbNumDocumento
            // 
            this.txbNumDocumento.Location = new System.Drawing.Point(186, 135);
            this.txbNumDocumento.Name = "txbNumDocumento";
            this.txbNumDocumento.Size = new System.Drawing.Size(232, 26);
            this.txbNumDocumento.TabIndex = 12;
            // 
            // txbTitular
            // 
            this.txbTitular.Location = new System.Drawing.Point(293, 333);
            this.txbTitular.Name = "txbTitular";
            this.txbTitular.Size = new System.Drawing.Size(49, 26);
            this.txbTitular.TabIndex = 16;
            // 
            // txbTotal
            // 
            this.txbTotal.Location = new System.Drawing.Point(574, 223);
            this.txbTotal.Name = "txbTotal";
            this.txbTotal.Size = new System.Drawing.Size(232, 26);
            this.txbTotal.TabIndex = 18;
            // 
            // cbxTransaccion
            // 
            this.cbxTransaccion.FormattingEnabled = true;
            this.cbxTransaccion.Items.AddRange(new object[] {
            "Venta",
            "Compra",
            "Encargo"});
            this.cbxTransaccion.Location = new System.Drawing.Point(190, 263);
            this.cbxTransaccion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxTransaccion.Name = "cbxTransaccion";
            this.cbxTransaccion.Size = new System.Drawing.Size(228, 28);
            this.cbxTransaccion.TabIndex = 19;
            // 
            // cbxFormaPago
            // 
            this.cbxFormaPago.FormattingEnabled = true;
            this.cbxFormaPago.Items.AddRange(new object[] {
            "Credito",
            "Contado",
            "Anticipo"});
            this.cbxFormaPago.Location = new System.Drawing.Point(574, 77);
            this.cbxFormaPago.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxFormaPago.Name = "cbxFormaPago";
            this.cbxFormaPago.Size = new System.Drawing.Size(228, 28);
            this.cbxFormaPago.TabIndex = 20;
            // 
            // cbxTipo
            // 
            this.cbxTipo.FormattingEnabled = true;
            this.cbxTipo.Items.AddRange(new object[] {
            "FACTURA",
            "CREDITO FISCAL",
            "TICKET"});
            this.cbxTipo.Location = new System.Drawing.Point(574, 156);
            this.cbxTipo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxTipo.Name = "cbxTipo";
            this.cbxTipo.Size = new System.Drawing.Size(228, 28);
            this.cbxTipo.TabIndex = 21;
            // 
            // dtpFecha
            // 
            this.dtpFecha.CustomFormat = "yyyy-MM-dd";
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha.Location = new System.Drawing.Point(184, 202);
            this.dtpFecha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(234, 26);
            this.dtpFecha.TabIndex = 22;
            // 
            // Notificador
            // 
            this.Notificador.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.Notificador.ContainerControl = this;
            // 
            // btnBuscarCliente
            // 
            this.btnBuscarCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(0)))), ((int)(((byte)(51)))));
            this.btnBuscarCliente.FlatAppearance.BorderSize = 0;
            this.btnBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarCliente.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarCliente.ForeColor = System.Drawing.Color.White;
            this.Notificador.SetIconAlignment(this.btnBuscarCliente, System.Windows.Forms.ErrorIconAlignment.TopRight);
            this.btnBuscarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarCliente.Image")));
            this.btnBuscarCliente.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscarCliente.Location = new System.Drawing.Point(74, 315);
            this.btnBuscarCliente.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBuscarCliente.Name = "btnBuscarCliente";
            this.btnBuscarCliente.Size = new System.Drawing.Size(138, 60);
            this.btnBuscarCliente.TabIndex = 23;
            this.btnBuscarCliente.Text = "Buscar";
            this.btnBuscarCliente.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscarCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuscarCliente.UseVisualStyleBackColor = false;
            this.btnBuscarCliente.Click += new System.EventHandler(this.btnBuscarCliente_Click);
            // 
            // btnNuevoTitular
            // 
            this.btnNuevoTitular.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNuevoTitular.BackgroundImage = global::General.Properties.Resources.cruz;
            this.btnNuevoTitular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNuevoTitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNuevoTitular.Location = new System.Drawing.Point(74, 375);
            this.btnNuevoTitular.Name = "btnNuevoTitular";
            this.btnNuevoTitular.Size = new System.Drawing.Size(144, 88);
            this.btnNuevoTitular.TabIndex = 24;
            this.btnNuevoTitular.UseVisualStyleBackColor = false;
            this.btnNuevoTitular.Click += new System.EventHandler(this.btnNuevoTitular_Click);
            // 
            // lblTitular
            // 
            this.lblTitular.AutoSize = true;
            this.lblTitular.Location = new System.Drawing.Point(289, 375);
            this.lblTitular.Name = "lblTitular";
            this.lblTitular.Size = new System.Drawing.Size(0, 20);
            this.lblTitular.TabIndex = 25;
            // 
            // EdicionDocumento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(870, 492);
            this.Controls.Add(this.lblTitular);
            this.Controls.Add(this.btnNuevoTitular);
            this.Controls.Add(this.btnBuscarCliente);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.cbxTipo);
            this.Controls.Add(this.cbxFormaPago);
            this.Controls.Add(this.cbxTransaccion);
            this.Controls.Add(this.txbTotal);
            this.Controls.Add(this.txbTitular);
            this.Controls.Add(this.txbNumDocumento);
            this.Controls.Add(this.txbIdDocumento);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Name = "EdicionDocumento";
            this.Text = "EdicionDocumento";
            this.Load += new System.EventHandler(this.EdicionDocumento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Notificador)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ErrorProvider Notificador;
        public System.Windows.Forms.TextBox txbIdDocumento;
        public System.Windows.Forms.TextBox txbNumDocumento;
        public System.Windows.Forms.TextBox txbTitular;
        public System.Windows.Forms.TextBox txbTotal;
        public System.Windows.Forms.ComboBox cbxTransaccion;
        public System.Windows.Forms.ComboBox cbxFormaPago;
        public System.Windows.Forms.ComboBox cbxTipo;
        public System.Windows.Forms.DateTimePicker dtpFecha;
        public System.Windows.Forms.Button btnBuscarCliente;
        private System.Windows.Forms.Button btnNuevoTitular;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblTitular;
    }
}