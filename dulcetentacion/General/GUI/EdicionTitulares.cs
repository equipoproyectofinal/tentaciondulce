﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionTitulares : Form
    {
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Titular oTitular = new CLS.Titular();
                oTitular.IdTitular = txbIdTitular.Text;
                oTitular.Tipo = cbxTipo.SelectedItem.ToString();
                oTitular.Nombres = txbNombres.Text;
                oTitular.Apellidos = txbApellidos.Text;
                oTitular.Direccion = txbDireccion.Text;
                oTitular.Telefono = txbTelefono.Text;
                oTitular.Dui = txbDui.Text;
                
                if (txbIdTitular.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oTitular.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oTitular.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }

        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (cbxTipo.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxTipo, "El campo no debe quedar vacio");
            }

            if (txbNombres.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbNombres, "El campo no puede quedar vacio");
            }
            if (txbApellidos.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbApellidos, "El campo no puede quedar vacio");
            }
            
            if (txbDireccion.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDireccion, "El campo no puede quedar vacio");
            }
            if (txbTelefono.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbTelefono, "El campo no puede quedar vacio");
            }
            if (txbDui.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDui, "El campo no puede quedar vacio");
            }            

            return Verificado;
        }

        public EdicionTitulares()
        {
            InitializeComponent();
        }
                
        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            Procesar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
