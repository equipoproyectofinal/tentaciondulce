﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class BuscarProducto : Form
    {
        BindingSource _Product = new BindingSource();
        CLS.Productos oProducto = new CLS.Productos();
        String idDocumento;

        public string IdDocumento { get => idDocumento; set => idDocumento = value; }

        private void CargarDatos()
        {
            try
            {
                _Product.DataSource = CacheManager.SystemCache.SOLO_PRODUCTOS_TERMINADOS();
                // FiltrarLocalmente();
                dtgvProductos.AutoGenerateColumns = false;
                dtgvProductos.DataSource = _Product;
                cbxImpuesto.SelectedIndex = 0;
            }
            catch
            {
            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.Text.Length > 0)
                {
                    _Product.Filter = "nombreProducto like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Product.RemoveFilter();
            }
            MessageBox.Show("LLEGUE AQUI");
            dtgvProductos.AutoGenerateColumns = false;
            dtgvProductos.DataSource = _Product;
        }

       

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }
        public BuscarProducto()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }
        public void ProductoSeleccionado()
        {
            oProducto.IdProducto = dtgvProductos.CurrentRow.Cells["idProducto"].Value.ToString();
            oProducto.NombreProducto = dtgvProductos.CurrentRow.Cells["nombreProducto"].Value.ToString();
            oProducto.PrecioSalida = double.Parse(dtgvProductos.CurrentRow.Cells["precioSalida"].Value.ToString());
            oProducto.Iva = float.Parse(dtgvProductos.CurrentRow.Cells["iva"].Value.ToString());

        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            ProductoSeleccionado();
            if (oProducto.GuardarDetalleVentas(IdDocumento, numericant.Value.ToString(), cbxImpuesto.SelectedItem.ToString(), txbSubtotal.Text, txbIvaCalculado.Text))
            {
                Close();
            }
            else
            {

            }
        }

        private void BuscarProducto_Load(object sender, EventArgs e)
        {
            CargarDatos();

        }

        public void CalcularTotal()
        {

                txbSubtotal.Text = Convert.ToDouble(dtgvProductos.CurrentRow.Cells[2].Value.ToString())*Convert.ToInt32(this.numericant.Value.ToString())+"";
                txbIvaCalculado.Text = Convert.ToDouble(txbSubtotal.Text)* Convert.ToDouble(dtgvProductos.CurrentRow.Cells[3].Value.ToString()) + "" ;
            
        }

        private void dtgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            CalcularTotal();
        }

        private void numericant_ValueChanged(object sender, EventArgs e)
        {
            CalcularTotal();
        }

        private void dtgvProductos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CalcularTotal();
        }

        private void dtgvProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CalcularTotal();
        }
    }
}
