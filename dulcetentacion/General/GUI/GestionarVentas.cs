﻿using General.CLS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CacheManager;



namespace General.GUI
{
    public partial class GestionarVentas : Form
    {
        BindingSource _Documento = new BindingSource();
        public GestionarVentas()
        {
            InitializeComponent();
            CargarDatos();
          
        }

        private void CargarDatos()
        {
            try
            {
                _Documento.DataSource = CacheManager.SystemCache.TODOS_LAS_VENTAS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Documento.Filter = "Cliente like '%" + txbFiltro.Text + "%'"; // OR fechaEncargo like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Documento.RemoveFilter();
            }
            dtgvVentas.AutoGenerateColumns = false;
            dtgvVentas.DataSource = _Documento;
            lblRegistros.Text = dtgvVentas.Rows.Count.ToString() + " Registros Encontrados";
        }
                       
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //General.GUI.GestionDocumentos f = new General.GUI.GestionDocumentos();
            //f.Show();
        }
        private void GestionarVentas_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }


        private void btnNuevaVenta_Click(object sender, EventArgs e)
        {
            General.GUI.EdicionDocumento n = new General.GUI.EdicionDocumento();
            n.ShowDialog();
            CargarDatos();
        }

        
        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea modificar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionDocumento f = new EdicionDocumento();

                f.txbIdDocumento.Text = dtgvVentas.CurrentRow.Cells["idDocumento"].Value.ToString();
                f.txbNumDocumento.Text = dtgvVentas.CurrentRow.Cells["numeroDocumento"].Value.ToString();
                f.dtpFecha.Text = dtgvVentas.CurrentRow.Cells["fecha"].Value.ToString();
                f.cbxTransaccion.Text = dtgvVentas.CurrentRow.Cells["transaccion"].Value.ToString();
                f.cbxFormaPago.Text = dtgvVentas.CurrentRow.Cells["formadePago"].Value.ToString();
                f.cbxTipo.Text = dtgvVentas.CurrentRow.Cells["tipo"].Value.ToString();
                f.txbTotal.Text = dtgvVentas.CurrentRow.Cells["total"].Value.ToString();
                f.lblTitular.Text = dtgvVentas.CurrentRow.Cells["Cliente"].Value.ToString();             
                         //f.btnBuscarCliente.Enabled = false;
                f.ShowDialog();
                CargarDatos();

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Documentos oDocumento = new CLS.Documentos();
                oDocumento.IdDocumento = dtgvVentas.CurrentRow.Cells["idDocumento"].Value.ToString();
                if (oDocumento.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnBuscarDetalle_Click(object sender, EventArgs e)
        {
            Documentos Ob = new Documentos();
            Ob.IdDocumento = dtgvVentas.CurrentRow.Cells["idDocumento"].Value.ToString();          
            Ob.IdTitular = dtgvVentas.CurrentRow.Cells["idTitular"].Value.ToString();
            Ob.NumeroDocumento = dtgvVentas.CurrentRow.Cells["numeroDocumento"].Value.ToString();
            DetalleVentas nuevo = new DetalleVentas();
            nuevo.ODocumento = Ob;
            nuevo.lblTitular.Text = dtgvVentas.CurrentRow.Cells["Cliente"].Value.ToString();
            nuevo.txbNumDoc.Text = dtgvVentas.CurrentRow.Cells["numeroDocumento"].Value.ToString();
            nuevo.ShowDialog();
            CargarDatos();
        }

        private void dtgvVentas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LblRegistros_Click(object sender, EventArgs e)
        {

        }
    }
}
