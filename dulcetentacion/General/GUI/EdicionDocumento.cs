﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionDocumento : Form
    {
        SessionManager.Sesion _SESION = SessionManager.Sesion.Instancia;
        CLS.Titular oTitular = new CLS.Titular();
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Documentos oDocumento = new CLS.Documentos();
                oDocumento.IdDocumento = txbIdDocumento.Text;
                oDocumento.NumeroDocumento = txbNumDocumento.Text;
                oDocumento.Fecha = dtpFecha.Text;
                oDocumento.Transaccion = cbxTransaccion.SelectedItem.ToString();
                oDocumento.FormaPago = cbxFormaPago.SelectedItem.ToString();
                oDocumento.IdTitular = txbTitular.Text;
                oDocumento.Tipo = cbxTipo.SelectedItem.ToString();
                oDocumento.Total = Convert.ToDouble (txbTotal.Text);

                if (txbIdDocumento.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oDocumento.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oDocumento.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }


        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbNumDocumento.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbNumDocumento, "El campo no puede quedar vacio");
            }
            if (dtpFecha.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpFecha, "El campo no puede quedar vacio");
            }
            //Verificar que el campo textlength en campo fecha da error, debe ir separado con punto
            if (cbxTransaccion.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxTransaccion, "El campo no puede quedar vacio");
            }
            if (cbxFormaPago.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxFormaPago, "El campo no puede quedar vacio");
            }
            if (txbTitular.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbTitular, "El campo no puede quedar vacio");
            }
            if (cbxTipo.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(cbxTipo, "El campo no puede quedar vacio");
            }
            if (txbTotal.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbTotal, "El campo no puede quedar vacio");
            }

            return Verificado;
        }           
                       

        public EdicionDocumento()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            Procesar();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {
            BuscarCliente nuevo = new BuscarCliente();
            nuevo.ShowDialog();
            this.oTitular = nuevo.OCliente;
            lblTitular.Text = oTitular.Nombres + " " + oTitular.Apellidos;
            txbTitular.Text = oTitular.IdTitular;
        }

        private void btnNuevoTitular_Click(object sender, EventArgs e)
        {
            General.GUI.EdicionTitulares nuevo = new General.GUI.EdicionTitulares();
            nuevo.Show();
        }

        private void EdicionDocumento_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
