﻿using CacheManager;
using General.CLS;
using REPORTES.GUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EntregarEncargo : Form
    {
        BindingSource _Productos = new BindingSource();
        Encargo oEncargo = new Encargo();
         internal Encargo OEncargo { get => oEncargo; set => oEncargo = value; }

        public EntregarEncargo()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TxbTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void EntregarEncargo_Load(object sender, EventArgs e)
        {
            CargarDatos();
            cbxFormaPago.SelectedIndex = 0;
            cbxTransaccion.SelectedIndex = 0;
            cbxTipo.SelectedIndex = 0;
            txbIdTitular.Text = oEncargo.IdTitular;
            txbNumDoc.Text = SystemCache.NuevoNumeroDocumento();

        }

        public void CalcularTotal()
        {
            double total = 0;
            double aux = 0;
            double impu = 0;
            //MessageBox.Show(dtgvDatos.Rows[0].Cells[2].Value.ToString());
            for (int i = 0; i < dtgvDatos.Rows.Count; i++)
            {
                aux = Convert.ToDouble(dtgvDatos.Rows[i].Cells[2].Value.ToString());
                total = total + aux;
                
                
            }
            txbTotal.Text = total+"";
        }

        private void CargarDatos()
        {
            try
            {
                _Productos.DataSource = oEncargo.ProductosEncargo();
                dtgvDatos.AutoGenerateColumns = false;
                dtgvDatos.DataSource = _Productos;
            }
            catch
            {

            }
            CalcularTotal();
        }

        private void BtnEntregar_Click(object sender, EventArgs e)
        {
            CLS.Documentos oDocumento = new CLS.Documentos();
            oDocumento.NumeroDocumento = txbNumDoc.Text;
            oDocumento.Fecha = dtpFecha.Text;
            oDocumento.Transaccion = cbxTransaccion.SelectedItem.ToString();
            oDocumento.FormaPago = cbxFormaPago.SelectedItem.ToString();
            oDocumento.IdTitular = txbIdTitular.Text;
            oDocumento.Tipo = cbxTipo.SelectedItem.ToString();
            oDocumento.Total = Convert.ToDouble(txbTotal.Text);
            if (oDocumento.Guardar())
            {
                MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CLS.Productos oProducto = new Productos();
                for (int i = 0; i < dtgvDatos.Rows.Count; i++)
                {
                    oProducto.IdProducto = dtgvDatos.Rows[i].Cells["idProducto"].Value.ToString();
                    oProducto.NombreProducto = dtgvDatos.Rows[i].Cells["nombreProducto"].Value.ToString();
                    oProducto.PrecioSalida = double.Parse(dtgvDatos.Rows[i].Cells["precioSalida"].Value.ToString());
                    oProducto.Iva = float.Parse(dtgvDatos.Rows[i].Cells["ivaCalculado"].Value.ToString());
                    String Cantidad = dtgvDatos.Rows[i].Cells["Cantidad"].Value.ToString();
                    String Subt= dtgvDatos.Rows[i].Cells["SubTotal"].Value.ToString();
                    String ivac= dtgvDatos.Rows[i].Cells["ivaCalculado"].Value.ToString();
                    String impu = dtgvDatos.Rows[i].Cells["exento"].Value.ToString();
                    String iddocument = SystemCache.IDDocuentoNumero(txbNumDoc.Text);
                    if (oProducto.GuardarDetalleVentas(iddocument, Cantidad, impu, Subt, ivac))
                    {
                        MessageBox.Show("Registros Detalles Guardados existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    else
                    {
                        MessageBox.Show("Error Detalles", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
                VisorReporteEncargo nue= new VisorReporteEncargo();
                nue.IdEncargo = oEncargo.IdEncargo;
                nue.ShowDialog();
                //Close();

            }

            else
            {
                MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
    }
}
