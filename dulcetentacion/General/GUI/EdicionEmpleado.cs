﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class EdicionEmpleado : Form
    {
        private void Procesar()
        {
            if (Verificacion())
            {
                CLS.Empleado oEmpleado = new CLS.Empleado();
                oEmpleado.IdEmpleado = txbIdEmpleado.Text;
                oEmpleado.Nombres = txbNombres.Text;
                oEmpleado.Apellidos = txbApellidos.Text;
                oEmpleado.Dui = txbDui.Text;
                oEmpleado.Direccion = txbDireccion.Text;
                oEmpleado.Telefono = txbTelefono.Text;
                oEmpleado.FechaNacimiento = dtpfechaNacimiento.Text;
                oEmpleado.Nit = txbNit.Text;
                if (txbIdEmpleado.TextLength == 0)
                {
                    //Es porque estoy guardadandoo
                    if (oEmpleado.Guardar())
                    {
                        MessageBox.Show("Registro Guardado existosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }

                    else
                    {
                        MessageBox.Show("El registro no se guardó", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    //estoy actualizando un registro
                    if (oEmpleado.Actualizar())
                    {
                        MessageBox.Show("Registro Actualizado", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("El registro no se actualizo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                }


            }
        }

        //Creamos un metodo de validacon. El id de empleado no va porque  puede estar en blanco al momento de inseratrlo
        private Boolean Verificacion()
        {

            Boolean Verificado = true;
            //Este es para que limpie todos los errores
            Notificador.Clear();
            if (txbNombres.TextLength <= 0)
            {
                Verificado = false;
                //Utilizamos el elemento erroProvider creado en el diseño
                Notificador.SetError(txbNombres, "El campo no puede quedar vacio");
            }
            if (txbApellidos.TextLength <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbApellidos, "El campo no puede quedar vacio");
            }
            //Verificar que el campo textlength en campo fecha da error, debe ir separado con punto
            if (txbDui.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDui, "El campo no puede quedar vacio");
            }
            if (txbDireccion.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbDireccion, "El campo no puede quedar vacio");
            }
            if (txbTelefono.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbTelefono, "El campo no puede quedar vacio");
            }
            if (dtpfechaNacimiento.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(dtpfechaNacimiento, "El campo no puede quedar vacio");
            }
            if (txbNit.Text.Length <= 0)
            {
                Verificado = false;
                Notificador.SetError(txbNit, "El campo no puede quedar vacio");
            }

            return Verificado;
        }
        public EdicionEmpleado()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Procesar();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
