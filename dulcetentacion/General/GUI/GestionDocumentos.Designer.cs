﻿namespace General.GUI
{
    partial class GestionDocumentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnEditarTitular = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAgregarTitular = new System.Windows.Forms.Button();
            this.btnBuscarTitular = new System.Windows.Forms.Button();
            this.cbxTransaccion = new System.Windows.Forms.ComboBox();
            this.cbxTipoTitular = new System.Windows.Forms.ComboBox();
            this.dtpxFechaDocumento = new System.Windows.Forms.DateTimePicker();
            this.txbTitular = new System.Windows.Forms.TextBox();
            this.txbNumDocumento = new System.Windows.Forms.TextBox();
            this.txbIdDocumento = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBuscarArticulo = new System.Windows.Forms.Button();
            this.txbIva = new System.Windows.Forms.TextBox();
            this.txbPrecioVenta = new System.Windows.Forms.TextBox();
            this.txbPrecioCompra = new System.Windows.Forms.TextBox();
            this.txbCantidad = new System.Windows.Forms.TextBox();
            this.txbProducto = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.dtgvDocumentos = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDocumentos)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnEditarTitular);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnAgregarTitular);
            this.panel1.Controls.Add(this.btnBuscarTitular);
            this.panel1.Controls.Add(this.cbxTransaccion);
            this.panel1.Controls.Add(this.cbxTipoTitular);
            this.panel1.Controls.Add(this.dtpxFechaDocumento);
            this.panel1.Controls.Add(this.txbTitular);
            this.panel1.Controls.Add(this.txbNumDocumento);
            this.panel1.Controls.Add(this.txbIdDocumento);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(821, 59);
            this.panel1.TabIndex = 1;
            // 
            // btnEditarTitular
            // 
            this.btnEditarTitular.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEditarTitular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarTitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarTitular.Location = new System.Drawing.Point(726, 3);
            this.btnEditarTitular.Margin = new System.Windows.Forms.Padding(2);
            this.btnEditarTitular.Name = "btnEditarTitular";
            this.btnEditarTitular.Size = new System.Drawing.Size(89, 54);
            this.btnEditarTitular.TabIndex = 19;
            this.btnEditarTitular.Text = "CAMBIAR";
            this.btnEditarTitular.UseVisualStyleBackColor = false;
            this.btnEditarTitular.Click += new System.EventHandler(this.btnEditarTitular_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "TIPO:";
            // 
            // btnAgregarTitular
            // 
            this.btnAgregarTitular.BackColor = System.Drawing.Color.YellowGreen;
            this.btnAgregarTitular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarTitular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarTitular.Location = new System.Drawing.Point(634, 2);
            this.btnAgregarTitular.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregarTitular.Name = "btnAgregarTitular";
            this.btnAgregarTitular.Size = new System.Drawing.Size(88, 56);
            this.btnAgregarTitular.TabIndex = 18;
            this.btnAgregarTitular.Text = "AGREGAR";
            this.btnAgregarTitular.UseVisualStyleBackColor = false;
            this.btnAgregarTitular.Click += new System.EventHandler(this.btnAgregarTitular_Click);
            // 
            // btnBuscarTitular
            // 
            this.btnBuscarTitular.BackgroundImage = global::General.Properties.Resources.IconoBuscar;
            this.btnBuscarTitular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBuscarTitular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarTitular.Location = new System.Drawing.Point(571, 2);
            this.btnBuscarTitular.Margin = new System.Windows.Forms.Padding(2);
            this.btnBuscarTitular.Name = "btnBuscarTitular";
            this.btnBuscarTitular.Size = new System.Drawing.Size(59, 55);
            this.btnBuscarTitular.TabIndex = 16;
            this.btnBuscarTitular.UseVisualStyleBackColor = true;
            this.btnBuscarTitular.Click += new System.EventHandler(this.btnBuscarTitular_Click);
            // 
            // cbxTransaccion
            // 
            this.cbxTransaccion.FormattingEnabled = true;
            this.cbxTransaccion.Items.AddRange(new object[] {
            "VENTA"});
            this.cbxTransaccion.Location = new System.Drawing.Point(388, 20);
            this.cbxTransaccion.Margin = new System.Windows.Forms.Padding(2);
            this.cbxTransaccion.Name = "cbxTransaccion";
            this.cbxTransaccion.Size = new System.Drawing.Size(82, 21);
            this.cbxTransaccion.TabIndex = 15;
            // 
            // cbxTipoTitular
            // 
            this.cbxTipoTitular.FormattingEnabled = true;
            this.cbxTipoTitular.Items.AddRange(new object[] {
            "CLIENTE"});
            this.cbxTipoTitular.Location = new System.Drawing.Point(222, 23);
            this.cbxTipoTitular.Margin = new System.Windows.Forms.Padding(2);
            this.cbxTipoTitular.Name = "cbxTipoTitular";
            this.cbxTipoTitular.Size = new System.Drawing.Size(82, 21);
            this.cbxTipoTitular.TabIndex = 14;
            // 
            // dtpxFechaDocumento
            // 
            this.dtpxFechaDocumento.CustomFormat = "yyyy-MM-dd";
            this.dtpxFechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpxFechaDocumento.Location = new System.Drawing.Point(385, -1);
            this.dtpxFechaDocumento.Margin = new System.Windows.Forms.Padding(2);
            this.dtpxFechaDocumento.Name = "dtpxFechaDocumento";
            this.dtpxFechaDocumento.Size = new System.Drawing.Size(97, 20);
            this.dtpxFechaDocumento.TabIndex = 13;
            // 
            // txbTitular
            // 
            this.txbTitular.Location = new System.Drawing.Point(222, -2);
            this.txbTitular.Margin = new System.Windows.Forms.Padding(2);
            this.txbTitular.Name = "txbTitular";
            this.txbTitular.Size = new System.Drawing.Size(111, 20);
            this.txbTitular.TabIndex = 12;
            // 
            // txbNumDocumento
            // 
            this.txbNumDocumento.Location = new System.Drawing.Point(59, 25);
            this.txbNumDocumento.Margin = new System.Windows.Forms.Padding(2);
            this.txbNumDocumento.Name = "txbNumDocumento";
            this.txbNumDocumento.Size = new System.Drawing.Size(92, 20);
            this.txbNumDocumento.TabIndex = 7;
            // 
            // txbIdDocumento
            // 
            this.txbIdDocumento.Location = new System.Drawing.Point(59, 1);
            this.txbIdDocumento.Margin = new System.Windows.Forms.Padding(2);
            this.txbIdDocumento.Name = "txbIdDocumento";
            this.txbIdDocumento.ReadOnly = true;
            this.txbIdDocumento.Size = new System.Drawing.Size(90, 20);
            this.txbIdDocumento.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(307, 27);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "TRANSACCION:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 1);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "FECHA:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(165, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "TITULAR:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "NUMERO:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID DCTO:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnBuscarArticulo);
            this.panel2.Controls.Add(this.txbIva);
            this.panel2.Controls.Add(this.txbPrecioVenta);
            this.panel2.Controls.Add(this.txbPrecioCompra);
            this.panel2.Controls.Add(this.txbCantidad);
            this.panel2.Controls.Add(this.txbProducto);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 59);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(821, 59);
            this.panel2.TabIndex = 2;
            // 
            // btnBuscarArticulo
            // 
            this.btnBuscarArticulo.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnBuscarArticulo.BackgroundImage = global::General.Properties.Resources.IconoBuscar;
            this.btnBuscarArticulo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBuscarArticulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarArticulo.Location = new System.Drawing.Point(584, -2);
            this.btnBuscarArticulo.Margin = new System.Windows.Forms.Padding(2);
            this.btnBuscarArticulo.Name = "btnBuscarArticulo";
            this.btnBuscarArticulo.Size = new System.Drawing.Size(46, 54);
            this.btnBuscarArticulo.TabIndex = 17;
            this.btnBuscarArticulo.Text = "BUSCAR";
            this.btnBuscarArticulo.UseVisualStyleBackColor = false;
            this.btnBuscarArticulo.Click += new System.EventHandler(this.btnBuscarArticulo_Click);
            // 
            // txbIva
            // 
            this.txbIva.Location = new System.Drawing.Point(463, 4);
            this.txbIva.Margin = new System.Windows.Forms.Padding(2);
            this.txbIva.Name = "txbIva";
            this.txbIva.Size = new System.Drawing.Size(89, 20);
            this.txbIva.TabIndex = 13;
            // 
            // txbPrecioVenta
            // 
            this.txbPrecioVenta.Location = new System.Drawing.Point(294, 35);
            this.txbPrecioVenta.Margin = new System.Windows.Forms.Padding(2);
            this.txbPrecioVenta.Name = "txbPrecioVenta";
            this.txbPrecioVenta.Size = new System.Drawing.Size(111, 20);
            this.txbPrecioVenta.TabIndex = 11;
            // 
            // txbPrecioCompra
            // 
            this.txbPrecioCompra.Location = new System.Drawing.Point(294, 6);
            this.txbPrecioCompra.Margin = new System.Windows.Forms.Padding(2);
            this.txbPrecioCompra.Name = "txbPrecioCompra";
            this.txbPrecioCompra.Size = new System.Drawing.Size(111, 20);
            this.txbPrecioCompra.TabIndex = 10;
            // 
            // txbCantidad
            // 
            this.txbCantidad.Location = new System.Drawing.Point(69, 27);
            this.txbCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.txbCantidad.Name = "txbCantidad";
            this.txbCantidad.Size = new System.Drawing.Size(89, 20);
            this.txbCantidad.TabIndex = 9;
            // 
            // txbProducto
            // 
            this.txbProducto.Location = new System.Drawing.Point(69, 3);
            this.txbProducto.Margin = new System.Windows.Forms.Padding(2);
            this.txbProducto.Name = "txbProducto";
            this.txbProducto.Size = new System.Drawing.Size(89, 20);
            this.txbProducto.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(428, 6);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "IVA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "P VENTA:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "P COMPRA:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 29);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "CANTIDAD:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 3);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "PRODUCTO:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnCancelar);
            this.panel3.Controls.Add(this.btnGuardar);
            this.panel3.Controls.Add(this.btnNuevo);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 384);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(821, 27);
            this.panel3.TabIndex = 3;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.Red;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Location = new System.Drawing.Point(554, 0);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(79, 21);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "CANCELAR";
            this.btnCancelar.UseVisualStyleBackColor = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Location = new System.Drawing.Point(464, 0);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(86, 21);
            this.btnGuardar.TabIndex = 4;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = false;
            // 
            // btnNuevo
            // 
            this.btnNuevo.BackColor = System.Drawing.Color.Lime;
            this.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevo.Location = new System.Drawing.Point(387, 0);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(2);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(73, 21);
            this.btnNuevo.TabIndex = 1;
            this.btnNuevo.Text = "NUEVO";
            this.btnNuevo.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(2, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Total a Pagar";
            // 
            // dtgvDocumentos
            // 
            this.dtgvDocumentos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgvDocumentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtgvDocumentos.Location = new System.Drawing.Point(0, 118);
            this.dtgvDocumentos.Margin = new System.Windows.Forms.Padding(2);
            this.dtgvDocumentos.Name = "dtgvDocumentos";
            this.dtgvDocumentos.RowTemplate.Height = 28;
            this.dtgvDocumentos.Size = new System.Drawing.Size(821, 266);
            this.dtgvDocumentos.TabIndex = 4;
            // 
            // GestionDocumentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 411);
            this.Controls.Add(this.dtgvDocumentos);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GestionDocumentos";
            this.Text = "Gestion de Documentos";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvDocumentos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBuscarTitular;
        private System.Windows.Forms.ComboBox cbxTransaccion;
        private System.Windows.Forms.ComboBox cbxTipoTitular;
        private System.Windows.Forms.DateTimePicker dtpxFechaDocumento;
        private System.Windows.Forms.TextBox txbTitular;
        private System.Windows.Forms.TextBox txbNumDocumento;
        private System.Windows.Forms.TextBox txbIdDocumento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnEditarTitular;
        private System.Windows.Forms.Button btnAgregarTitular;
        private System.Windows.Forms.Button btnBuscarArticulo;
        private System.Windows.Forms.TextBox txbIva;
        private System.Windows.Forms.TextBox txbPrecioVenta;
        private System.Windows.Forms.TextBox txbPrecioCompra;
        private System.Windows.Forms.TextBox txbCantidad;
        private System.Windows.Forms.TextBox txbProducto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dtgvDocumentos;
        private System.Windows.Forms.Label label4;
    }
}