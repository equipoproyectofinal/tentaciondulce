﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General.GUI
{
    public partial class GestiondeEmpleados : Form
    {
        BindingSource _Empleados = new BindingSource();
        
        public GestiondeEmpleados()
        {
            InitializeComponent();
        }

        private void CargarDatos()
        {
            try
            {
                _Empleados.DataSource = CacheManager.SystemCache.TODOS_LOS_EMPLEADOS();
                FiltrarLocalmente();
            }
            catch
            {

            }
        }

        private void FiltrarLocalmente()
        {
            try
            {
                if (txbFiltro.TextLength > 0)
                {
                    _Empleados.Filter = "nombres like '%" + txbFiltro.Text + "%' OR apellidos like '%" + txbFiltro.Text + "%'";
                }

            }
            catch
            {
                _Empleados.RemoveFilter();
            }
            dtgvDatos.AutoGenerateColumns = false;
            dtgvDatos.DataSource = _Empleados;
            lblRegistros.Text = dtgvDatos.Rows.Count.ToString() + " Registros Encontrados";
        }

        private void dtgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GestiondeUsuarios_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void txbFiltro_TextChanged(object sender, EventArgs e)
        {
            FiltrarLocalmente();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            EdicionEmpleado nuevo = new EdicionEmpleado();
            nuevo.ShowDialog();
            CargarDatos();
           
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea editar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //estamos sincronizando la interfaz grafica con el registro seleccionado 
                EdicionEmpleado f = new EdicionEmpleado();

                f.txbIdEmpleado.Text = dtgvDatos.CurrentRow.Cells["idEmpleado"].Value.ToString();
                f.txbNombres.Text = dtgvDatos.CurrentRow.Cells["Nombres"].Value.ToString();
                f.txbApellidos.Text = dtgvDatos.CurrentRow.Cells["Apellidos"].Value.ToString();
                f.txbDui.Text = dtgvDatos.CurrentRow.Cells["DUI"].Value.ToString();
                f.txbDireccion.Text = dtgvDatos.CurrentRow.Cells["Telefono"].Value.ToString();
                f.txbTelefono.Text = dtgvDatos.CurrentRow.Cells["Direccion"].Value.ToString();
                f.txbTelefono.Text = dtgvDatos.CurrentRow.Cells["fechaNacimiento"].Value.ToString();
                f.txbTelefono.Text = dtgvDatos.CurrentRow.Cells["nit"].Value.ToString();

                f.ShowDialog();
                CargarDatos();

            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Realmente desea Eliminar el registro seleccionado?", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                CLS.Empleado oEmpleado = new CLS.Empleado();
                oEmpleado.IdEmpleado = dtgvDatos.CurrentRow.Cells["idEmpleado"].Value.ToString();
                if (oEmpleado.Eliminar())
                {
                    MessageBox.Show("Registro eliminado exitosamente", "Confirmacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("Registro no se eliminó exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {

        }
    }
}
